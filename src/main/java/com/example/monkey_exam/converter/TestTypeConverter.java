package com.example.monkey_exam.converter;

import java.util.HashMap;
import java.util.Map;

public class TestTypeConverter {

        private static Map<String, Integer> typeMap = new HashMap<>();

        static {
            typeMap.put("单选", 1);
            typeMap.put("多选", 2);
            typeMap.put("判断", 3);
            typeMap.put("填空", 4);
            typeMap.put("简答", 5);
        }

        public static Integer convertType(String type) {
            return typeMap.getOrDefault(type, -1);
        }

    }
