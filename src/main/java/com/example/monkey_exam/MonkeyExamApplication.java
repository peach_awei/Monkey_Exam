package com.example.monkey_exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication
@EnableOpenApi
public class MonkeyExamApplication {
    public static void main(String[] args) {
        SpringApplication.run(MonkeyExamApplication.class, args);
    }
}
