package com.example.monkey_exam.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页查询结果封装类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
//使用mybatis使用的分页查询封装类
public class PageBean {
    private Long total;  //总记录数（共几条）
    private List rows;   //当前页的数据列表
}
