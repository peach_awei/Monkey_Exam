package com.example.monkey_exam.entity.dto;

import com.example.monkey_exam.entity.pojo.Answer;
import com.example.monkey_exam.entity.pojo.Record;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerRecordDTO {
    private Record record;
    private List<Answer> answer;
}
