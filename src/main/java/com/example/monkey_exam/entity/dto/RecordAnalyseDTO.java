package com.example.monkey_exam.entity.dto;

import com.example.monkey_exam.entity.vo.ExamAnalyseVo;
import com.example.monkey_exam.entity.vo.RecordAnalyseVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class RecordAnalyseDTO<T> {

    private ExamAnalyseVo examAnalyse;

    private RecordAnalyseVo recordAnalyse;

}
