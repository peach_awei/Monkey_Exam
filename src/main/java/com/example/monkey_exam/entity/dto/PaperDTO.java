package com.example.monkey_exam.entity.dto;

import com.example.monkey_exam.entity.pojo.Paper;
import lombok.Data;

import java.util.List;

@Data
public class PaperDTO {
    private Paper paper ;
    private List<Integer> grade;
    private List<Integer> checkList ;
    private List<Integer> checkBoxList ;
    private List<Integer> checkPanList ;
    private List<Integer> checkTianList ;
    private List<Integer> checkJianList ;
}
