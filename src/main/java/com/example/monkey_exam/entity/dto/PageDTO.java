package com.example.monkey_exam.entity.dto;

import lombok.Data;

import java.util.List;

@Data
//分页查询的封装dto
public class PageDTO<T> {

    private Long total; //总条数

    private Long pages;  //总页数

    private List<T> list;  //集合
}
