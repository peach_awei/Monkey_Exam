package com.example.monkey_exam.entity.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
//答题卡封装类
public class SheetDTO<Map> {
    //答题卡信息
    private Object examInfo;

    //答题卡集合
    private List cardList;
}
