package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Bank {

    @TableId(value = "bank_test_id", type = IdType.AUTO)
    private int bankTestId;

    @TableField("bank_teacher_id")
    private int bankTeacherId;

    @TableField("bank_test_time")
    private LocalDateTime bankTestTime;

    @TableField("bank_test_name")
    private String bankTestName;

    @TableField("bank_test_type")
    private String bankTestType;

    @TableField("bank_test_subject")
    private String bankTestSubject;

    @TableField("bank_test_answer")
    private String bankTestAnswer;

    @TableField("bank_test_section_A")
    private String bankTestSectionA;

    @TableField("bank_test_section_B")
    private String bankTestSectionB;

    @TableField("bank_test_section_C")
    private String bankTestSectionC;

    @TableField("bank_test_section_D")
    private String bankTestSectionD;

    @TableField("bank_test_section_E")
    private String bankTestSectionE;

    @TableField("bank_test_section_F")
    private String bankTestSectionF;

    @TableField("bank_test_section_G")
    private String bankTestSectionG;

    @TableField(exist = false)
    private String answerYour;

    @TableField("bank_test_analyze")
    private String bankTestAnalyze;

    @TableField(exist = false)
    private String answerGrade;
}
