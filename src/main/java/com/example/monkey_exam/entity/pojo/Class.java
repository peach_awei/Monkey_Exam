package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
@TableName("class")
public class Class {
    @TableField("class_name")
    private String className;
    @TableId(value = "class_id")
    private int classId;
    @TableField("class_num")
    private int classNum;
    @TableField("class_teacher_id")
    private int classTeacherId;
    @TableField("class_invite")
    private int classInvite;
}
