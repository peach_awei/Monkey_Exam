package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Student {

    @TableField(exist = false)
    private String account;
    @TableId(value = "stu_id")
    private String stuId;
    @TableField(exist = false)
    private String code;
    @TableField("stu_name")
    private String stuName;
    @TableField("stu_phone")
    private String stuPhone;
    @TableField("stu_password")
    private String stuPassword;
    @TableField("stu_age")
    private int stuAge;
    @TableField("stu_gender")
    private int stuGender;
    @TableField("stu_class")
    private int stuClass;
    @TableField(exist = false)
    private String invite;
}
