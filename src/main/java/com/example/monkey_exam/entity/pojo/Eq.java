package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Eq {
    @TableId("eq_id")
    private int eqId;

    @TableField("eq_paper_id")
    private int eqPaperId;

    @TableField("eq_test_id")
    private int eqTestId;

    @TableField("eq_test_type")
    private int eqTestType;

    @TableField("eq_test_grade")
    private int eqTestGrade;

}
