package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Record {
    @TableId("record_id")
    private int recordId;

    @TableField("record_stu_id")
    private int recordStuId;

    @TableField(exist = false)
    private String stuName;

    @TableField("record_exam_id")
    private int recordExamId;

    @TableField(exist = false)
    private String exName;

    @TableField("record_teacher_id")
    private int recordTeacherId;

    @TableField(exist = false)
    private String teacherName;

    @TableField("record_status")
    private int recordStatus;

    @TableField("record_mark_time")
    private LocalDateTime recordMarkTime;

    @TableField("record_pass_grade")
    private int recordPassGrade;

    @TableField("record_grade")
    private Double recordGrade;

    @TableField("record_start_time")
    private LocalDateTime recordStartTime;

    @TableField("record_end_time")
    private LocalDateTime recordEndTime;

}
