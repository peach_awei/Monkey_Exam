package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Answer {
    @TableId(value = "answer_id")
    private int answerId;
    @TableField("answer_stu_id")
    private int answerStuId;
    @TableField("answer_exam_id")
    private int answerExamId;
    @TableField("answer_paper_id")
    private int answerPaperId;
    @TableField("answer_test_id")
    private int answerTestId;
    @TableField("answer_type")
    private String answerType;
    @TableField("answer_right")
    private String answerRight;
    @TableField("answer_your")
    private String answerYour;
    @TableField(exist = false)
    private Double answerPass;
    @TableField("answer_grade")
    private Double answerGrade;
}
