package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Paper {
    @TableId("paper_id")
    private int paperId;

    @TableField("paper_name")
    private String paperName;

    @TableField("paper_teacher_id")
    private int paperTeacherId;

    @TableField("paper_create_time")
    private LocalDateTime paperCreateTime;

    @TableField("paper_pass_grade")
    private int paperPassGrade;

    @TableField("paper_grade")
    private int paperGrade;
}
