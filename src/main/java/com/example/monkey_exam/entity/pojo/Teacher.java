package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Teacher {
    @TableId(value = "tea_id")
    private String teaId;
    @TableField("teacher_name")
    private String teacherName;
    @TableField("teacher_password")
    private String teacherPassword;
    @TableField("teacher_status")
    private Integer teacherStatus;
    @TableField("teacher_age")
    private int teacherAge;
    @TableField("teacher_gender")
    private Integer teacherGender;

}
