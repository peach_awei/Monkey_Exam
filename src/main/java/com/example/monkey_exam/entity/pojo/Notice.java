package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Notice {
    @TableId("notice_id")
    private int noticeId;

    @TableField("notice_headline")
    private String noticeHeadline;

    @TableField("notice_content")
    private String noticeContent;

    @TableField("notice_level")
    private int noticeLevel;

    @TableField("notice_teacher_id")
    private int noticeTeacherId;

    @TableField("notice_status")
    private int noticeStatus;

    @TableField("notice_class_id")
    private int noticeClassId;

    @TableField("notice_time")
    private LocalDateTime noticeTime;

    @TableField("notice_end_time")
    private LocalDateTime noticeEndTime;
}
