package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Exam {
    @TableId(value = "ex_id")
    private int exId;
    @TableField("ex_name")
    private String exName;
    @TableField("ex_paper_Id")
    private int exPaperId;
    @TableField("ex_teacher_Id")
    private int exTeacherId;
    @TableField(exist = false)
    private String teacherName;
    @TableField("ex_class_id")
    private int exClassId;
    @TableField("ex_time")
    private LocalDateTime exTime;
    @TableField("ex_endtime")
    private LocalDateTime exEndtime;
}
