package com.example.monkey_exam.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class Certificate {
    @TableId("cert_id")
    private int certId;

    @TableField("cert_stu_id")
    private int certStuId;

    @TableField("cert_stu_name")
    private String certStuName;

    @TableField("cert_content")
    private String certContent;

    @TableField("cert_ward_name")
    private String certWardName;

    @TableField("cert_tea_id")
    private int certTeaId;

    @TableField("cert_tea_name")
    private String certTeaName;

    @TableField("cert_time")
    private LocalDateTime certTime;

    @TableField("cert_file")
    private String certFile;
}
