package com.example.monkey_exam.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class RecordAnalyseVo {

    //    统计信息
//    考试id
    private Integer recordExamId;

//    考试平均分
    private Integer averageScore;

    //    考试最高分
    private Integer maxScore;

    //    考试最低分
    private Integer minScore;

    //    参与人数
    private Integer participantCount;

    //    及格人数
    private Integer passCount;

    //    及格率
    private String passRate;




}
