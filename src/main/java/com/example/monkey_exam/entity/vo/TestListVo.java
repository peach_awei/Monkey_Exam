package com.example.monkey_exam.entity.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造

//该类用于展示答题卡

public class TestListVo {

    private int bankTestId;

    private String bankTestName;

    private int eqTestGrade;

    private String bankTestType;

    private String bankTestSectionA;

    private String bankTestSectionB;

    private String bankTestSectionC;

    private String bankTestSectionD;

    private String bankTestSectionE;

    private String bankTestSectionF;

    private String bankTestSectionG;

}
