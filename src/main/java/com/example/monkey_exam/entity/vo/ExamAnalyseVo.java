package com.example.monkey_exam.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class ExamAnalyseVo {
    //    考试信息
//    考试名
    private String exName;

    //    班级id
    private Integer exClassId;

    //    班级名
    private String className;

    //    考试总分
    private Integer paperPassGrade;

    //    及格分数
    private Integer paperGrade;

}
