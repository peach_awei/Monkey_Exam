package com.example.monkey_exam.entity.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class ParticipationVo {
    private LocalDate time;
    private Integer num;
}
