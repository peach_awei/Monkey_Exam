package com.example.monkey_exam.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造

//该类用于recordController 的examList返回数据

public class ExamListVo {
    private int exId;
    private String exName ;
    private int exPaperId;
    private int paperPassGrade;
    private int paperGrade;
    private LocalDateTime recordEndTime;
    private int recordStatus;
}
