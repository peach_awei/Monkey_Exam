package com.example.monkey_exam.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造

//该类用于成绩排名返回
public class RecordRankVo {
    private int recordStuId;
    private int recordExamId;
    private int recordGrade;
    private int recordRank;
}
