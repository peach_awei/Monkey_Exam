package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Record;
import com.example.monkey_exam.entity.vo.ExamListVo;
import com.example.monkey_exam.entity.vo.ParticipationVo;
import com.example.monkey_exam.entity.vo.RecordAnalyseVo;
import com.example.monkey_exam.entity.vo.RecordRankVo;

import java.util.List;
import java.util.Map;

public interface RecordService extends IService<Record> {
    PageDTO<Record> pageFindWho(Integer page, Integer pagesize, Integer exid);

    Map<String, Object> findCard(Integer stuid, Integer exid);

    int changeStatus(Integer stuid, Integer exid, Double grade);

    PageDTO<ExamListVo> examList(Integer page, Integer pagesize, Integer stuid);

    Integer searchRecentExam(Integer stuid);

    List<RecordRankVo> searchRank(Integer stuid, Integer exid);

    boolean addRecord(Record record);

    List<ParticipationVo> Participation(Integer teaid);

    RecordAnalyseVo recordAnalyse(Integer exId, Integer paperId);

    Map<String,Integer> gradeList(Integer exId);
}
