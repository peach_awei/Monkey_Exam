package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Class;
import com.example.monkey_exam.entity.pojo.Student;

import java.util.List;


public interface ClassService extends IService<Class> {
    int insertClass(Class c);

    int delClass(Integer id);

    int updateClassNameById(Integer id,String name);

    List<Class> selectClass(Integer teaid);

    PageDTO<Student> Page(Integer page, Integer pagesize, Integer classid);

    Class selectById(Integer classId);

    Integer minusNum(int stuClass);

    Integer addNum(int stuClass);

    Integer getClassId(String invite);
}
