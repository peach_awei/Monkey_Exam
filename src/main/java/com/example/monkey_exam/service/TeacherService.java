package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Teacher;

import java.util.List;
import java.util.Map;

public interface TeacherService extends IService<Teacher> {
    int newTeacher(Teacher teacher);

    int myInformation(Integer tid, Integer age, Integer gender);

    PageDTO pageteacher(Integer page, Integer pagesize, Integer teaId, String teaName, String gender);

    int rePassword(List<Integer> tids);

    int changePsw(Map<String ,Object> teacher);

    Teacher myInforWithPsw(Integer teaId);

}
