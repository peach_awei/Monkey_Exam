package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.pojo.Eq;

import java.util.LinkedHashMap;
import java.util.List;

public interface EqService extends IService<Eq> {
    int insertEq(List<Eq> eq);

    int deleteById(Integer eqid, Integer paperid);

    List<LinkedHashMap<String,Object>> selectTestById(Integer paperid);

    int insertOne(Eq eq);
}
