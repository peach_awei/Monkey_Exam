package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.pojo.Notice;

import java.util.LinkedHashMap;
import java.util.List;

public interface NoticeService extends IService<Notice> {
    List<LinkedHashMap<String, Object>> selectall(Integer classid);

    int newNotice(Notice notice);

    int changeNotice(Notice notice);

    int delete(Integer noticeid);

    int updateStatus(Integer classid);

    List<Notice> findNotice(Integer classid);

    Notice oneNotice(Integer noticeid);
}
