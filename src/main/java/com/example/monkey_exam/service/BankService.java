package com.example.monkey_exam.service;


import com.example.monkey_exam.entity.dto.PageBean;
import com.example.monkey_exam.entity.pojo.Bank;

import java.util.List;

public interface BankService {

    PageBean page(Integer page, Integer pagesize, Integer teaid, String name, Integer type, String subject);

    Integer DeleteTestById(List<Integer> ids);

    int AddTestByTeaid(Bank bank);

    Bank SearchTestById(Integer tid,Integer teaid);

    int UpdateTestByTid(Bank bank);

    List<Bank> SeacheAllTest(Integer teaId);

    int countAll(Integer teaId);

    List<String> getType();
}
