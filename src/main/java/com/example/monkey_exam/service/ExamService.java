package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.entity.vo.ExamAnalyseVo;

import java.util.LinkedHashMap;
import java.util.List;

public interface ExamService extends IService<Exam> {
    int AddExam(Exam exam);

    PageDTO<Exam> Page(Integer page, Integer pageSize, Integer tid, String exName, Integer classId, String startTime);

    Integer del(Integer exId);

    Integer updateByexId(Exam exam);

    Long countAll(String teaid);

    List<Exam> countExaming(Integer teaid);

    PageDTO<Exam> PageFindExamed(Integer page, Integer pagesize, Integer teaid);

    List<Exam> selectExam(Integer classid);

    LinkedHashMap<String, Object> findExamInfo(Integer exid);

    List<LinkedHashMap<String,Object>> selectExamed(Integer stuId);

    PageDTO<Exam> pageAnalyseExam(Integer page, Integer pageSize, Integer teaId, String exName, Integer classId, String startDate, String endDate);

    ExamAnalyseVo analyseExam(Integer exId);
}
