package com.example.monkey_exam.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Certificate;
import com.example.monkey_exam.mapper.CertificateMapper;
import com.example.monkey_exam.service.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class CertificateServiceImpl extends ServiceImpl<CertificateMapper, Certificate> implements CertificateService {

    @Autowired
    CertificateMapper certificateMapper;

    @Override
    public boolean insert(Certificate certificate) {
        return this.save(certificate);
    }

    @Override
    public Integer Del(Integer certid) {
        if( this.removeById(certid)){
            return 1;
        }else {
            return 0;
        }
    }

    @Override
    public PageDTO<Certificate> getAllCert(Integer page, Integer pageSize, Integer certStuId) {
        Page<Certificate> pages = new Page<>(page, pageSize);
        QueryWrapper<Certificate> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cert_stu_id",certStuId);
        Page<Certificate> p = certificateMapper.selectPage(pages, queryWrapper);
        // 4.封装结果
        PageDTO<Certificate> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Certificate> records = p.getRecords();  //获取到条件查询到的数据，变成list集合
        // 5.判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        6.封装List
//        BeanUtil：用于在不同类型的对象之间进行转换，例如将数据库查询结果转换为DTO（数据传输对象）列表。
        dto.setList(BeanUtil.copyToList(records, Certificate.class));
        return dto;
    }

    @Override
    public Certificate getCert(Integer certId) {
        QueryWrapper<Certificate> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cert_id",certId);
        return certificateMapper.selectOne(queryWrapper);
    }

    @Override
    public Integer updateFile(Integer certId, String file) {
        UpdateWrapper<Certificate> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("cert_id",certId)
                .set("cert_file",file);
        return certificateMapper.update(null,updateWrapper);
    }

}
