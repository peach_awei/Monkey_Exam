package com.example.monkey_exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.pojo.Eq;
import com.example.monkey_exam.mapper.EqMapper;
import com.example.monkey_exam.service.EqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service
public class EqServiceImpl  extends ServiceImpl<EqMapper, Eq> implements EqService {

    @Autowired
    EqMapper eqMapper;

    @Override
    public int insertEq(List<Eq> eq) {
        int i =0;
        for (Eq eqList : eq){
            eqMapper.insert(eqList);
            i++;
        }
        return i;
    }

    @Override
    public int deleteById(Integer eqid, Integer paperid) {
        QueryWrapper queryWrapper = new QueryWrapper<>()
                .eq("eq_test_id",eqid)
                .eq("eq_paper_id",paperid);
        return eqMapper.delete(queryWrapper);
    }

    @Override
    public List<LinkedHashMap<String,Object>> selectTestById(Integer paperid) {
        return eqMapper.selectTestById(paperid);
    }

    @Override
    public int insertOne(Eq eq) {
        return eqMapper.insert(eq);
    }

}
