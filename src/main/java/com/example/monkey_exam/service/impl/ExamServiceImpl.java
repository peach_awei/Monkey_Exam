package com.example.monkey_exam.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.entity.vo.ExamAnalyseVo;
import com.example.monkey_exam.mapper.ExamMapper;
import com.example.monkey_exam.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class ExamServiceImpl extends ServiceImpl<ExamMapper, Exam> implements ExamService {

    @Autowired
    ExamMapper examMapper;

    @Override
    public int AddExam(Exam exam) {
        return examMapper.insert(exam);
    }


    @Override
    public PageDTO<Exam> Page( Integer page, Integer pageSize,Integer tid, String exName, Integer classId, String startTime) {
        // 1.构建分页条件
        Page<Exam> pages = new Page<>(page, pageSize);
        // 2.查询排序条件
        pages.addOrder(OrderItem.asc("ex_time"));
        // 3.查询
        LambdaQueryWrapper<Exam> queryWrapper = new LambdaQueryWrapper<>();
        if (tid != null) {
            queryWrapper.like(Exam::getExTeacherId, tid);
        }
        if (exName != null) {
            queryWrapper.like(Exam::getExName, exName);
        }
        if (classId != null) {
            queryWrapper.eq(Exam::getExClassId, classId);
        }
        if (startTime != null) {
            queryWrapper.like(Exam::getExTime, startTime);
        }
        Page<Exam> p = examMapper.selectPage(pages, queryWrapper);
        // 4.封装结果
        PageDTO<Exam> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Exam> records = p.getRecords();  //获取到条件查询到的数据，变成list集合
        // 5.判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        6.封装List
//        BeanUtil：用于在不同类型的对象之间进行转换，例如将数据库查询结果转换为DTO（数据传输对象）列表。
        dto.setList(BeanUtil.copyToList(records, Exam.class));
        return dto;
    }


    @Override
    public Integer del(Integer exId) {
        return examMapper.deleteById(exId);
    }


    @Override
    public Integer updateByexId(Exam exam) {
        return examMapper.updateById(exam);
    }


    @Override
    public Long countAll(String teaid) {
        QueryWrapper queryWrapper = new QueryWrapper<>()
                .eq("ex_teacher_id",teaid);
        return examMapper.selectCount(queryWrapper);
    }


    @Override
    public List<Exam> countExaming(Integer teaid) {
        QueryWrapper queryWrapper= new QueryWrapper<>()
                .eq("ex_teacher_id", teaid)
                .lt("ex_time", LocalDateTime.now())
                .gt("ex_endtime",LocalDateTime.now());
        return examMapper.selectList(queryWrapper);
    }

    @Override
    public PageDTO<Exam> PageFindExamed(Integer page, Integer pagesize, Integer teaid) {
        // 1.构建分页条件
        Page<Exam> pages = new Page<>(page, pagesize);
        // 2.查询排序条件
        pages.addOrder(OrderItem.asc("ex_time"));
        // 3.查询
        QueryWrapper<Exam> queryWrapper= new QueryWrapper<>();
        if (teaid != null) {
            queryWrapper.eq("ex_teacher_id", teaid);
            queryWrapper.lt("ex_endtime", LocalDateTime.now());
        }
        Page<Exam> p = examMapper.selectPage(pages, queryWrapper);
        List<Exam> record = examMapper.selectExamList(teaid);
        // 4.封装结果
        PageDTO<Exam> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Exam> records = record;  //获取到条件查询到的数据，变成list集合
        // 5.判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        6.封装List
//        BeanUtil：用于在不同类型的对象之间进行转换，例如将数据库查询结果转换为DTO（数据传输对象）列表。
        dto.setList(BeanUtil.copyToList(records, Exam.class));
        return dto;
    }

    @Override
    public List<Exam> selectExam(Integer classid) {
        QueryWrapper<Exam> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ex_class_id", classid);
//        考试开始时间 < 当前时间 < 考试结束时间
//        ex_time < LocalDateTime.now() < ex_end_time
//        gt：（“a”，b） a > b
//        lt：（“a”，b） a < b
        queryWrapper.gt("ex_endtime", LocalDateTime.now());
        return examMapper.selectList(queryWrapper);
    }

    @Override
    public LinkedHashMap<String, Object> findExamInfo(Integer exid) {
        return examMapper.findExamInfo(exid);
    }

    @Override
    public List<LinkedHashMap<String,Object>> selectExamed(Integer stuId) {
        return examMapper.selectExam(stuId);
    }

    @Override
    public PageDTO<Exam> pageAnalyseExam(Integer page, Integer pageSize, Integer teaId, String exName, Integer classId, String startDate, String endDate) {
        // 1.构建分页条件
        Page<Exam> pages = new Page<>(page, pageSize);
        // 2.查询排序条件
        pages.addOrder(OrderItem.asc("ex_time"));
        // 3.查询
        QueryWrapper<Exam> queryWrapper= new QueryWrapper<>();
        queryWrapper.eq("ex_teacher_id", teaId);
        if (exName != null && !exName.equals("")) {
            queryWrapper.like("ex_name", exName);
        }if (classId != null) {
            queryWrapper.eq("ex_class_id", classId);
        }if (startDate != null && endDate != null) {
            queryWrapper.gt("ex_time", startDate);  //>
            queryWrapper.lt("ex_endtime", endDate);   //<
        }

        Page<Exam> p = examMapper.selectPage(pages, queryWrapper);
        // 4.封装结果
        PageDTO<Exam> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Exam> records = p.getRecords();  //获取到条件查询到的数据，变成list集合
        // 5.判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        6.封装List
//        BeanUtil：用于在不同类型的对象之间进行转换，例如将数据库查询结果转换为DTO（数据传输对象）列表。
        dto.setList(BeanUtil.copyToList(records, Exam.class));
        return dto;
    }

    @Override
    public ExamAnalyseVo analyseExam(Integer exId) {
        return examMapper.analyzeExam(exId);
    }

}
