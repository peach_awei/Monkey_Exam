package com.example.monkey_exam.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Student;
import com.example.monkey_exam.mapper.ClassMapper;
import com.example.monkey_exam.mapper.StudentMapper;
import com.example.monkey_exam.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {

    @Autowired
    StudentMapper studentMapper;

    @Autowired
    ClassMapper classMapper;


    @Override
    public int register(Student student) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = passwordEncoder.encode(student.getStuPassword());
        student.setStuPassword(password);
        return studentMapper.insert(student);
    }

    @Override
    public int updatePsw(List<Integer> stuids, Integer stuid, String status) {
        UpdateWrapper<Student> updateWrapper = new UpdateWrapper<>();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (stuid == null) {    //初始化密码
            updateWrapper.in("stu_id", stuids)
                         .set("stu_password", passwordEncoder.encode("123456"));
        }
        if (stuids == null) {  //修改个人密码
            updateWrapper.eq("stu_id", stuid)
                         .set("stu_password", passwordEncoder.encode(status));
        }
        return studentMapper.update(null,updateWrapper);
    }


    @Override
    public List<Map<String,Object>> selectByClassId(Integer classid) {
        return studentMapper.selectUserList(classid);
    }


    @Override
    public Integer changeClass(Integer stuid, Integer status) {
        UpdateWrapper<Student> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("stu_id", stuid)
                     .set("stu_class", status);
        return studentMapper.update(null, updateWrapper);
    }


    @Override
    public Student selectOnesInfo(Integer stuid) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("stu_id",stuid);
        return studentMapper.selectOne(queryWrapper);
    }

    @Override
    public PageDTO<Student> userPage(Integer page, Integer pageSize, Integer tid, String sname, Integer classid, Integer gender) {
        // 1.构建分页条件
        Page<Student> pages = new Page<>(page, pageSize);
        // 3.查询
//        3.1查询我管理的班级
        List<Integer> list = classMapper.selectId(tid);
//        3.2查询班级下的所有学生
        LambdaQueryWrapper<Student> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        if (classid != null) {
            lambdaQueryWrapper.eq(Student::getStuClass, classid);
        }else {
            lambdaQueryWrapper.in(Student::getStuClass, list);
        }
        if (sname != null) {
            lambdaQueryWrapper.like(Student::getStuName, sname);
        }
        if (gender != null) {
            lambdaQueryWrapper.eq(Student::getStuGender, gender);
        }

        Page<Student> p = studentMapper.selectPage(pages, lambdaQueryWrapper);
        // 4.封装结果
        PageDTO<Student> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Student> records = p.getRecords();  //获取到条件查询到的数据，变成list集合
        // 5.判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        6.封装List
//        BeanUtil：用于在不同类型的对象之间进行转换，例如将数据库查询结果转换为DTO（数据传输对象）列表。
        dto.setList(BeanUtil.copyToList(records, Student.class));
        return dto;
    }

    @Override
    public String selectOne(String phone) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("stu_phone",phone);
        return this.getOne(queryWrapper).getStuId();
    }

    @Override
    public Student selectOneExit(Integer stuId, Integer stuPhone) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("stu_id",stuId)
                .eq("stu_phone", stuPhone);
        return this.getOne(queryWrapper);
    }

    @Override
    public List<Map<String, Object>> memberList(Integer classId) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("stu_class",classId);
        queryWrapper.select("stu_id","stu_name");
        return studentMapper.selectMaps(queryWrapper);
    }

    @Override
    public Student selectOneInfor(String username) {
        if(username.length()==11){
            QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("stu_phone",username);
            queryWrapper.select("stu_id","stu_name","stu_class");
            return studentMapper.selectOne(queryWrapper);
        }else {
            QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("stu_id",username);
            queryWrapper.select("stu_id","stu_name","stu_class");
            return studentMapper.selectOne(queryWrapper);
        }
    }

}
