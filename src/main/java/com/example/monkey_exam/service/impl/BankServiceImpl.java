package com.example.monkey_exam.service.impl;

import com.example.monkey_exam.entity.dto.PageBean;
import com.example.monkey_exam.entity.pojo.Bank;
import com.example.monkey_exam.mapper.BankMapper;
import com.example.monkey_exam.service.BankService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BankServiceImpl implements BankService {

    private final BankMapper bankMapper;

    public BankServiceImpl(BankMapper bankMapper) {
        this.bankMapper = bankMapper;
    }


//    分页查询（方法一）
/*  @Override
    public PageBean page(Integer page, Integer pagesize) {
//        1.获取总记录数
        Long count = bankMapper.count();
//        2.获取分页查询的数据
        List<Bank> bankList = bankMapper.page((page - 1) * pagesize, pagesize);
//        3.封装
        PageBean pageBean = new PageBean(count,bankList);
        return pageBean;
    }
*/


//    分页查询（方法二：使用pageHelper）
    @Override
    public PageBean page(Integer page, Integer pagesize, Integer teaid, String name, Integer type, String subject) {
//        1.设置分页参数
        PageHelper.startPage(page,pagesize);

//        2.执行查询
        List<Bank> bankList = bankMapper.List(teaid,name,type,subject);
        System.out.println("teaid:"+teaid);

        if(type != null){
            // 创建映射
            Map<Integer, String> typeMap = new HashMap<>();
            typeMap.put(1, "单选");
            typeMap.put(2, "多选");
            typeMap.put(3, "判断");
            typeMap.put(4, "填空");
            typeMap.put(5, "简答");

            // 遍历bankList
            for (Bank bank : bankList) {
                // 获取type属性
                int types = Integer.parseInt(bank.getBankTestType());
                // 使用映射将type转换为文字类型
                String typeStr = typeMap.get(types);
                // 设置新的type值
                bank.setBankTestType(typeStr);
            }
        }

        Page<Bank> P = (Page<Bank>) bankList;

//        3.封装
        PageBean pageBean = new PageBean(P.getTotal(), P.getResult());
        return pageBean;
    }


    //    通过id删除题库中的题
    @Override
    public Integer DeleteTestById(List<Integer> ids) {
        return bankMapper.DeleteTestById(ids);
    }

//    新增题目
    @Override
    public int AddTestByTeaid(Bank bank) {
        bank.setBankTestTime(LocalDateTime.now());
        int ids = bankMapper.AddTestByTeaid(bank);
        System.out.println("ids:"+ids);
        return bank.getBankTestId();
    }

//    查找
    @Override
    public Bank SearchTestById(Integer tid,Integer teaid) {
        return bankMapper.SearchTestById(tid,teaid);
    }

    //
    @Override
    public int UpdateTestByTid(Bank bank) {
        return bankMapper.UpdateTestByTid(bank);
    }

    @Override
    public List<Bank> SeacheAllTest(Integer teaId) {
        return bankMapper.SearchAllTest(teaId);
    }

    @Override
    public int countAll(Integer teaId) {
        return bankMapper.count(teaId);
    }

    @Override
    public List<String> getType() {
        return bankMapper.getType();
    }
}
