package com.example.monkey_exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.pojo.Notice;
import com.example.monkey_exam.mapper.NoticeMapper;
import com.example.monkey_exam.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Autowired
    NoticeMapper noticeMapper;

    @Override
    public List<LinkedHashMap<String, Object>> selectall(Integer classid) {
        return noticeMapper.selectAll(classid);
    }

    @Override
    public int newNotice(Notice notice) {
        notice.setNoticeTime(LocalDateTime.now());
        return noticeMapper.insert(notice);
    }

    @Override
    public int changeNotice(Notice notice) {
        return noticeMapper.updateById(notice);
    }

    @Override
    public int delete(Integer noticeid) {
        return noticeMapper.deleteById(noticeid);
    }

    @Override
    public int updateStatus(Integer classid) {
        UpdateWrapper<Notice> noticeUpdateWrapper = new UpdateWrapper<>();
        noticeUpdateWrapper.lt("notice_end_time", LocalDateTime.now());
        noticeUpdateWrapper.set("notice_status", 1);
        return noticeMapper.update(null,noticeUpdateWrapper);
    }

    @Override
    public List<Notice> findNotice(Integer classid) {
        QueryWrapper<Notice> noticeQueryWrapper = new QueryWrapper<>();
        noticeQueryWrapper.eq("notice_class_id", classid);
        noticeQueryWrapper.eq("notice_status", 0);
        return noticeMapper.selectList(noticeQueryWrapper);
    }

    @Override
    public Notice oneNotice(Integer noticeid) {
        QueryWrapper<Notice> noticeQueryWrapper = new QueryWrapper<>();
        noticeQueryWrapper.eq("notice_id",noticeid);
        return noticeMapper.selectOne(noticeQueryWrapper);
    }
}
