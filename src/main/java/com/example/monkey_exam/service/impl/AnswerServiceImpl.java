package com.example.monkey_exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.pojo.Answer;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.mapper.*;
import com.example.monkey_exam.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper,Answer> implements AnswerService {

    @Autowired
    AnswerMapper answerMapper;

    @Autowired
    ExamMapper examMapper;

    @Autowired
    RecordMapper recordMapper;

    @Autowired
    BankMapper bankMapper;

    @Autowired
    EqMapper eqMapper;

    @Override
    public List<Exam> selectExam(Integer stuid) {
        QueryWrapper<Exam> queryWrapper = new QueryWrapper();
        queryWrapper.inSql("ex_id","select answer_exam_id from answer where answer_stu_id = "+ stuid );
        List<Exam> exams = examMapper.selectList(queryWrapper);
        return exams;
    }

    @Override
    public List<LinkedHashMap<String, Object>> findZero(Integer stuid, Integer exid) {
//        通过用户id和考场id，查询本场考试的错题
        return answerMapper.findZero(stuid,exid);
    }

    @Override
    public List<Map> cardInfo(Integer stuid, Integer exid, Integer only) {
        return answerMapper.cardInfo(stuid,exid,only);
    }

    @Override
    public int changeGrade(Integer stuid, Integer exId, Integer testid, Double grade) {
        QueryWrapper<Answer> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("answer_stu_id", stuid);
        queryWrapper.eq("answer_exam_id",exId);
        queryWrapper.eq("answer_test_id", testid);
        Answer answer = answerMapper.selectOne(queryWrapper);

        answer.setAnswerGrade(grade);
        return answerMapper.updateById(answer);
    }

    @Override
    public int Answering(List<Answer> answer) {
        int i = 0;
        for (Answer answerlist : answer) {
            String right = bankMapper.getRight(answerlist.getAnswerTestId());
            answerlist.setAnswerRight(right);
            if (Objects.equals(answerlist.getAnswerType(), "1") || Objects.equals(answerlist.getAnswerType(), "2") || Objects.equals(answerlist.getAnswerType(), "3")|| Objects.equals(answerlist.getAnswerType(), "4")){
                if (right.equals(answerlist.getAnswerYour())) {
                    answerlist.setAnswerGrade(eqMapper.getGrade(answerlist.getAnswerTestId(),answerlist.getAnswerPaperId()));
                } else {
                    answerlist.setAnswerGrade(0.0);
                }
                answerMapper.insert(answerlist);
                i++;
            }else {
                answerlist.setAnswerRight("A");
                answerlist.setAnswerGrade(0.0);
                answerMapper.insert(answerlist);
                i++;
            }
        }
        return i;
    }

    @Override
    public Double sumGrade(Integer recordStuId, Integer recordExamId) {
        return answerMapper.sumGrade(recordStuId,recordExamId);
    }
}
