package com.example.monkey_exam.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Class;
import com.example.monkey_exam.entity.pojo.Student;
import com.example.monkey_exam.mapper.ClassMapper;
import com.example.monkey_exam.mapper.StudentMapper;
import com.example.monkey_exam.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;


@Service
public class ClassServiceImpl extends ServiceImpl<ClassMapper, Class> implements ClassService {

    @Autowired
    ClassMapper classMapper;

    @Autowired
    StudentMapper studentMapper;

    @Override
    public int insertClass(Class c) {
        c.setClassNum(0);
        return classMapper.insert(c);
    }


    @Override
    public int delClass(Integer id) {
        QueryWrapper<Class> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("class_id",id);
        return classMapper.delete(queryWrapper);

    }


    @Override
    public int updateClassNameById(Integer id,String name) {
        UpdateWrapper<Class> wrapper=new UpdateWrapper<>();
        wrapper.set("class_name", name).eq("class_id", id);
        return classMapper.update(null,wrapper);
    }


    @Override
    public List<Class> selectClass(Integer teaid) {
        QueryWrapper<Class> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("class_teacher_id",teaid);
        return classMapper.selectList(queryWrapper);
    }


    @Override
    public PageDTO<Student> Page(Integer page, Integer pagesize, Integer classid) {
        // 1.构建分页条件
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Student> pages = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(page, pagesize);
        // 2.查询排序条件
        pages.addOrder(OrderItem.asc("stu_name"));
        // 3.查询
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        if (classid != null) {
            queryWrapper.eq("stu_class", classid);
        }
        Page<Student> p = studentMapper.selectPage(pages, queryWrapper);
        // 4.封装结果
        PageDTO<Student> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Student> records = p.getRecords();  //获取到条件查询到的数据，变成list集合
        // 5.判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        6.封装List
//        BeanUtil：用于在不同类型的对象之间进行转换，例如将数据库查询结果转换为DTO（数据传输对象）列表。
        dto.setList(BeanUtil.copyToList(records, Student.class));
        return dto;
    }

    @Override
    public Class selectById(Integer classId) {
        return this.getById(classId);
    }

    @Override
    public Integer minusNum(int stuClass) {
        return classMapper.minusNum(stuClass);
    }

    @Override
    public Integer addNum(int stuClass) {
        return classMapper.addNum(stuClass);
    }

    @Override
    public Integer getClassId(String invite) {
        QueryWrapper<Class> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("class_invite",invite);
        return classMapper.selectOne(queryWrapper).getClassId();
    }
}
