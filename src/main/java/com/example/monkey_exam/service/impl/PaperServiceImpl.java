package com.example.monkey_exam.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Paper;
import com.example.monkey_exam.mapper.PaperMapper;
import com.example.monkey_exam.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Service
public class PaperServiceImpl extends ServiceImpl<PaperMapper,Paper> implements PaperService {

    @Autowired
    PaperMapper paperMapper;


    @Override
    public PageDTO<Paper> Page(Integer page, Integer pageSize, String paperName, LocalDateTime startTime, LocalDateTime endTime) {
        // 1.构建分页条件
        Page<Paper> pages = new Page<>(page, pageSize);
        // 2.查询排序条件
        pages.addOrder(OrderItem.asc("paper_create_time"));
        // 3.查询
        LambdaQueryWrapper<Paper> queryWrapper = new LambdaQueryWrapper<>();
        if (paperName != null) {
            queryWrapper.like(Paper::getPaperName, paperName);
        }
        if (startTime != null) {
            queryWrapper.between(Paper::getPaperCreateTime, startTime, endTime);
        }
        Page<Paper> p = paperMapper.selectPage(pages, queryWrapper);
        // 4.封装结果
        PageDTO<Paper> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Paper> records = p.getRecords();  //获取到条件查询到的数据，变成list集合
        // 5.判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        6.封装List
//        BeanUtil：用于在不同类型的对象之间进行转换，例如将数据库查询结果转换为DTO（数据传输对象）列表。
        dto.setList(BeanUtil.copyToList(records, Paper.class));
        return dto;
    }

    @Override
    public int deletePaper(Integer paperId) {
        return paperMapper.deleteById(paperId);
    }

    @Override
    public Integer insertPaper(Paper paper) {
        paperMapper.insertPaper(paper);
        Integer paperId = paper.getPaperId();
        return paperId;
    }

    @Override
    public int updatePaper(Paper paper) {
        return paperMapper.updateById(paper);
    }

    @Override
    public List<Paper> allPaper(Integer teaId) {
        QueryWrapper<Paper> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("paper_teacher_id",teaId);
        return paperMapper.selectList(queryWrapper);
    }


}
