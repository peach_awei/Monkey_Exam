package com.example.monkey_exam.service.impl;

import com.example.monkey_exam.Util.JwtUtil;
import com.example.monkey_exam.Util.RedisCache;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.LoginStudent;
import com.example.monkey_exam.entity.pojo.LoginTeacher;
import com.example.monkey_exam.entity.pojo.Student;
import com.example.monkey_exam.entity.pojo.Teacher;
import com.example.monkey_exam.service.LoginService;
import com.example.monkey_exam.service.StudentService;
import com.example.monkey_exam.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Service
//写登录的核心代码
public class LoginServiceImpl implements LoginService {

    @Autowired
    TeacherService teacherService;

    @Autowired
    StudentService studentService;

    @Autowired
    //先在SecurityConfig，使用@Bean注解重写官方的authenticationManagerBean类，然后这里才能注入成功
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Override
    public Result loginStudent(Student student) {

        String username = student.getAccount();
        String password = student.getStuPassword();

        UsernamePasswordAuthenticationToken authenticationToken;
        String stuId;
        String classId;
        Student student1 = studentService.selectOneInfor(username);
        classId = String.valueOf(student1.getStuClass());
        String Name = student1.getStuName();

        if (username.length() == 11) {
            stuId = student1.getStuId();
            // 使用电话号码登录
            authenticationToken = new UsernamePasswordAuthenticationToken(student.getAccount(), password);
        } else {
            stuId = username;
            // 使用学号登录
            authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        }

        //获取AuthenticationManager的authenticate方法来进行用户认证
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);

        //判断上面那行的authenticate是否为null，如果是则认证没通过，就抛出异常
        if(Objects.isNull(authenticate)){
            throw new RuntimeException("登录失败");
        }

        //如果认证通过，就使用userid生成一个jwt，然后把jwt存入ResponseResult后返回
        LoginStudent loginStudent = (LoginStudent) authenticate.getPrincipal();
        String userid = loginStudent.getStudent().getStuId();
//        生成token
        String jwt = JwtUtil.createJWT(userid);

        //把完整的用户信息存入redis，其中userid作为key，注意存入redis的时候加了前缀 login:
        Map<String, String> map = new HashMap<>();
        map.put("token",jwt);
        map.put("information",stuId);
        map.put("classId",classId);
        map.put("Name",Name);
        redisCache.setCacheObject("login:" + userid, loginStudent);
        return new Result(200,"登录成功（学生）",map);
    }


    @Override
    public Result loginTeacher(Teacher teacher) {
        //用户在登录页面输入的用户名和密码
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(teacher.getTeaId(),teacher.getTeacherPassword());

        //获取AuthenticationManager的authenticate方法来进行用户认证
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);

        //判断上面那行的authenticate是否为null，如果是则认证没通过，就抛出异常
        if(Objects.isNull(authenticate)){
            throw new RuntimeException("登录失败");
        }

        //如果认证通过，就使用userid生成一个jwt，然后把jwt存入ResponseResult后返回
        LoginTeacher loginTeacher = (LoginTeacher) authenticate.getPrincipal();
        String userid = loginTeacher.getTeacher().getTeaId();
//        生成token
        String jwt = JwtUtil.createJWT(userid);

        //把完整的用户信息存入redis，其中userid作为key，注意存入redis的时候加了前缀 login:
        Map<String, String> map = new HashMap<>();
        map.put("token",jwt);
        map.put("information", teacher.getTeaId());
        redisCache.setCacheObject("login:"+userid,loginTeacher);
        return new Result(200,"登录成功(老师)",map);
    }

    //-----------------------------------退出登录的具体代码--------------------------------

    @Override
    public Result logoutStudent() {
        //获取我们在JwtAuthenticationTokenFilter类写的SecurityContextHolder对象中的用户id
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        //loginUser是我们在domain目录写好的实体类
        LoginStudent loginStudent = (LoginStudent) authentication.getPrincipal();
        //获取用户id
        String userid = loginStudent.getStudent().getStuId();

        //根据用户id，删除redis中的token值，注意我们的key是被 login: 拼接过的，所以下面写完整key的时候要带上 longin:
        redisCache.deleteObject("login:" + userid);

        return new Result(200,"注销成功","null");
    }

    @Override
    public Result logoutTeacher() {
        //获取我们在JwtAuthenticationTokenFilter类写的SecurityContextHolder对象中的用户id
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        //loginUser是我们在domain目录写好的实体类
        LoginTeacher loginTeacher = (LoginTeacher) authentication.getPrincipal();
        //获取用户id
        String userid = loginTeacher.getTeacher().getTeaId();

        //根据用户id，删除redis中的token值，注意我们的key是被 login: 拼接过的，所以下面写完整key的时候要带上 longin:
        redisCache.deleteObject("login:" + userid);

        return new Result(200,"注销成功","null");
    }

}