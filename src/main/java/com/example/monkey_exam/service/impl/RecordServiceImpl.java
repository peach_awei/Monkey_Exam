package com.example.monkey_exam.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Record;
import com.example.monkey_exam.entity.vo.ExamListVo;
import com.example.monkey_exam.entity.vo.ParticipationVo;
import com.example.monkey_exam.entity.vo.RecordAnalyseVo;
import com.example.monkey_exam.entity.vo.RecordRankVo;
import com.example.monkey_exam.mapper.AnswerMapper;
import com.example.monkey_exam.mapper.RecordMapper;
import com.example.monkey_exam.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class RecordServiceImpl extends ServiceImpl<RecordMapper, Record> implements RecordService {

    @Autowired
    RecordMapper recordMapper;

    @Autowired
    AnswerMapper answerMapper;

    @Override
    public PageDTO<Record> pageFindWho(Integer page, Integer pagesize, Integer exid) {
//        构建分页条件
        Page<Record> pages = new Page<>(page, pagesize);
// 3.查询
        QueryWrapper<Record> queryWrapper= new QueryWrapper<>();
        if (exid != null) {
            queryWrapper.eq("record_exam_id", exid);
        }
        Page<Record> p = recordMapper.selectPage(pages, queryWrapper);
        List<Record> userByAge = recordMapper.pageFindWho(exid);

        PageDTO<Record> dto = new PageDTO<>();
        dto.setTotal(p.getTotal());  //总条数
        dto.setPages(p.getPages());  //总页数
        List<Record> records = userByAge;  // 获取到条件查询到的数据，变成list集合
        // 判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        ?
        dto.setList(BeanUtil.copyToList(records, Record.class));
        return dto;
    }

    @Override
    public Map<String, Object> findCard(Integer stuid, Integer exid) {
        return recordMapper.selectCard(stuid,exid);
    }

    @Override
    public int changeStatus(Integer stuid, Integer exid, Double grade) {
        QueryWrapper<Record> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("record_stu_id", stuid);
        queryWrapper.eq("record_exam_id", exid);
        Record record = recordMapper.selectOne(queryWrapper);
        record.setRecordStatus(1);
        record.setRecordMarkTime(LocalDateTime.now());
        record.setRecordGrade(grade);
        return recordMapper.updateById(record);
    }

    @Override
    public PageDTO<ExamListVo> examList(Integer page, Integer pagesize, Integer stuid) {
        Page<ExamListVo> page1 = new Page<>(page, pagesize);
        IPage<ExamListVo> userByAge = recordMapper.examList(page1, stuid);
        Page<ExamListVo> pages = (Page<ExamListVo>) userByAge;

        PageDTO<ExamListVo> dto = new PageDTO<>();
        dto.setTotal(pages.getTotal());  //总条数
        dto.setPages(pages.getPages());  //总页数
        List<ExamListVo> records = pages.getRecords();  // 获取到条件查询到的数据，变成list集合
        // 判断查询到的结果是否为空
        if (CollUtil.isEmpty(records)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
//        ?
        dto.setList(records);
        return dto;
    }

    @Override
    public Integer searchRecentExam(Integer stuid) {
        return recordMapper.searchRecentExam(stuid);
    }

    @Override
    public List<RecordRankVo> searchRank(Integer stuid, Integer exid) {
        return recordMapper.searchRank(stuid,exid);
    }

    @Override
    public boolean addRecord(Record record) {
        return this.save(record);
    }

    @Override
    public List<ParticipationVo> Participation(Integer teaid) {
        return recordMapper.participation(teaid);
    }

    @Override
    public RecordAnalyseVo recordAnalyse(Integer exId, Integer paperId) {
        return recordMapper.recordAnalyse(exId,paperId);
    }

    @Override
    public Map<String,Integer> gradeList(Integer exId) {
        return recordMapper.gradeList(exId);
    }
}
