package com.example.monkey_exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.monkey_exam.entity.pojo.LoginStudent;
import com.example.monkey_exam.entity.pojo.LoginTeacher;
import com.example.monkey_exam.entity.pojo.Student;
import com.example.monkey_exam.entity.pojo.Teacher;
import com.example.monkey_exam.mapper.StudentMapper;
import com.example.monkey_exam.mapper.TeacherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class MyUserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private TeacherMapper teacherMapper;


    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
        Student student;
        if(id.length() == 11){
            student = studentMapper.selectOne(new LambdaQueryWrapper<Student>().eq(Student::getStuPhone, id));
        }else {
            student = studentMapper.selectOne(new LambdaQueryWrapper<Student>().eq(Student::getStuId, id));
        }

        Teacher teacher = teacherMapper.selectOne(new LambdaQueryWrapper<Teacher>().eq(Teacher::getTeaId, id));

        if (Objects.isNull(student) && Objects.isNull(teacher)) {
            throw new RuntimeException("用户名或者密码错误");
        }

        //------------------------------------------------------------------------------

        //把查询到的user结果，封装成UserDetails类型，然后返回。
        //但是由于UserDetails是个接口，所以我们先需要在domino目录新建LoginUser类，作为UserDetails的实现类，再写下面那行
        if (Objects.nonNull(student)) {
            List<String> list = Arrays.asList("student");
            return new LoginStudent(student,list); //这里传了第二个参数，表示的是权限信息
        } else {
            QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("tea_id",teacher.getTeaId());
            Teacher teacher1 = teacherMapper.selectOne(queryWrapper);
            List<String> list;
            if(teacher1.getTeacherStatus()==1){
                 list = Arrays.asList("admin","teacher");
            }else {
                 list = Arrays.asList("teacher");
            }
            return new LoginTeacher(teacher,list); //这里传了第二个参数，表示的是权限信息
        }

    }
}
