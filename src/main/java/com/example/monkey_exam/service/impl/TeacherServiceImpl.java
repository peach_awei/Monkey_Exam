package com.example.monkey_exam.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Teacher;
import com.example.monkey_exam.mapper.TeacherMapper;
import com.example.monkey_exam.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService  {

    @Autowired
    TeacherMapper teacherMapper;

    @Override
    public int newTeacher(Teacher teacher) {
        this.save(teacher);
        return 1;
    }

    @Override
    public int myInformation(Integer tid, Integer age, Integer gender) {
        UpdateWrapper<Teacher> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("tea_id",tid)
                .set("teacher_age",age)
                .set("teacher_gender",gender);
        this.update(updateWrapper);
        return 1;
    }

    @Override
    public PageDTO<Teacher> pageteacher(Integer page, Integer pagesize, Integer teaId, String teaName, String gender) {
        Page<Teacher> page1 = new Page<>(page, pagesize);
        QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("teacher_status", 0);
        if(teaId != null){
            queryWrapper.eq("tea_id",teaId);
        }if(teaName != null && !teaName.isEmpty()){
            queryWrapper.like("teacher_name",teaName);
        }if(gender != null && !gender.isEmpty()){
            queryWrapper.eq("teacher_gender",gender);
        }
        Page<Teacher> pages = teacherMapper.selectPage(page1, queryWrapper);

        PageDTO<Teacher> dto = new PageDTO<>();
        dto.setTotal(pages.getTotal());  //总条数
        dto.setPages(pages.getPages());  //总页数
        List<Teacher> teachers = pages.getRecords();  // 获取到条件查询到的数据，变成list集合
        // 判断查询到的结果是否为空
        if (CollUtil.isEmpty(teachers)) {
            dto.setList(Collections.emptyList());
            return dto;
        }
        dto.setList(BeanUtil.copyToList(teachers, Teacher.class));
        return dto;
    }

    @Override
    public int rePassword(List<Integer> tids) {
        for (Integer teaId : tids) {
            UpdateWrapper<Teacher> updateWrapper = new UpdateWrapper<>();
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String newPassword = passwordEncoder.encode("123456");
            updateWrapper.eq("tea_id", teaId).set("teacher_password", newPassword);
            this.update(updateWrapper);
        }
        return tids.size();

    }

    @Override
    public int changePsw(Map<String ,Object> teacher) {
        UpdateWrapper<Teacher> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("tea_id",teacher.get("Id"))
                .set("teacher_password", teacher.get("Password"));
                this.update(updateWrapper);
        return 1;
    }

    @Override
    public Teacher myInforWithPsw(Integer teaId) {
        QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tea_id",teaId);
        queryWrapper.select("teacher_password");
        return teacherMapper.selectOne(queryWrapper);
    }

}
