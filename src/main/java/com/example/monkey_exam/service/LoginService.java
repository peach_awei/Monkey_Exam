package com.example.monkey_exam.service;

import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Student;
import com.example.monkey_exam.entity.pojo.Teacher;
import org.springframework.stereotype.Service;

@Service
public interface LoginService {
    Result loginStudent(Student student);

    Result loginTeacher(Teacher teacher);

    Result logoutStudent();

    Result logoutTeacher();
}
