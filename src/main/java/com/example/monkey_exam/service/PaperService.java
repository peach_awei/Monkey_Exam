package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Paper;

import java.time.LocalDateTime;
import java.util.List;

public interface PaperService extends IService<Paper> {

    PageDTO<Paper> Page(Integer page, Integer pageSize, String paperName, LocalDateTime startTime, LocalDateTime endTime);

    int deletePaper(Integer paperId);

    Integer insertPaper(Paper paper);

    int updatePaper(Paper paper);

    List<Paper> allPaper(Integer teaId);
}
