package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.pojo.Answer;
import com.example.monkey_exam.entity.pojo.Exam;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface AnswerService extends IService<Answer> {

     List<Exam> selectExam(Integer stuid);

     List<LinkedHashMap<String, Object>> findZero(Integer stuid, Integer exid);

     List<Map> cardInfo(Integer stuid, Integer exid, Integer only);

     int changeGrade(Integer stuid, Integer exId, Integer testid, Double grade);

    int Answering(List<Answer> answer);

    Double sumGrade(Integer recordStuId, Integer recordExamId);
}
