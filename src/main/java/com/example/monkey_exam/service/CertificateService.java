package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Certificate;
import org.springframework.stereotype.Service;

@Service
public interface CertificateService  extends IService<Certificate> {

    boolean insert(Certificate certificate);

    Integer Del(Integer certid);

    PageDTO<Certificate> getAllCert(Integer page, Integer pageSize, Integer certStuId);

    Certificate getCert(Integer certId);

    Integer updateFile(Integer certId, String file);

}
