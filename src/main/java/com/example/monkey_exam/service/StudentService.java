package com.example.monkey_exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.pojo.Student;

import java.util.List;
import java.util.Map;

public interface StudentService extends IService<Student> {
    int register(Student student);

    int updatePsw(List<Integer> stuids,Integer stuid, String status);

    List<Map<String,Object>> selectByClassId(Integer classid);

    Integer changeClass(Integer stuid, Integer status);

    Student selectOnesInfo(Integer stuid);

//    用于统计分析
    PageDTO<Student> userPage(Integer page, Integer pageSize, Integer tid, String sname, Integer classid, Integer gender);

    String selectOne(String phone);

    Student selectOneExit(Integer stuId, Integer stuPhone);

    List<Map<String, Object>> memberList(Integer classId);

    Student selectOneInfor(String username);
}
