package com.example.monkey_exam.filter;

import com.example.monkey_exam.Util.JwtUtil;
import com.example.monkey_exam.Util.RedisCache;
import com.example.monkey_exam.entity.pojo.LoginStudent;
import com.example.monkey_exam.entity.pojo.LoginTeacher;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private RedisCache redisCache;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        //        if (request.getHeader("Auth") == null){
//            filterChain.doFilter(request,response);
//            return;
//        }

        //获取token，指定你要获取的请求头叫什么
        String token = request.getHeader("token");
        if (!StringUtils.hasText(token)) {
            //如果请求没有携带token
            filterChain.doFilter(request, response);
            return;
        }

        //解析token
        String userid;
        try {
            Claims claims = JwtUtil.parseJWT(token);
            userid = claims.getSubject();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("token非法");
        }
        //从redis中获取用户信息
        String redisKey = "login:" + userid;
        LoginStudent loginStudent = null;
        LoginTeacher loginTeacher = null;
        if (userid.length()>5) {
            loginStudent = redisCache.getCacheObject(redisKey);
            if(Objects.isNull(loginStudent)){
                //判断获取到的用户信息是否为空，因为redis里面可能并不存在这个用户信息，例如缓存过期了
                throw new RuntimeException("学生未登录");
            }
        }else {
            loginTeacher = redisCache.getCacheObject(redisKey);
            if(Objects.isNull(loginTeacher)){
                //判断获取到的用户信息是否为空，因为redis里面可能并不存在这个用户信息，例如缓存过期了
                throw new RuntimeException("教师未登录");
            }
        }

        //把最终的LoginUser用户信息，通过setAuthentication方法，存入SecurityContextHolder
        //TODO 获取权限信息封装到Authentication中
        UsernamePasswordAuthenticationToken authenticationToken ;

        //第一个参数是LoginUser用户信息，第二个参数是凭证(null)，第三个参数是权限信息
        if (userid.length()>5) {
            authenticationToken = new UsernamePasswordAuthenticationToken(loginStudent, null, loginStudent.getAuthorities());
        }else {
            authenticationToken = new UsernamePasswordAuthenticationToken(loginTeacher, null, loginTeacher.getAuthorities());
        }

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        //全部做完之后，就放行
        filterChain.doFilter(request, response);
    }
}