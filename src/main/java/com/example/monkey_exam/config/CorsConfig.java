package com.example.monkey_exam.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//跨域解决方案
public class CorsConfig implements WebMvcConfigurer {
//理解CORS：CORS是一种W3C标准，它允许服务器在响应头中添加特定的HTTP头，以此来告诉浏览器某个网站中的资源可以被其他域下的页面访问。
//配置CORS头信息：在后端项目中，可以通过配置响应头Access-Control-Allow-Origin来指定哪些域名可以访问当前资源的URL。例如，将其设置为*表示接受所有来源的请求。
    @Override
    //重写spring提供的WebMvcConfigurer接口的addCorsMappings方法
    public void addCorsMappings(CorsRegistry registry) {
        // 设置允许跨域的路径
        registry.addMapping("/**")
                // 设置允许跨域请求的域名
                .allowedOrigins("*")
                // 是否允许cookie
                .allowCredentials(true)
                // 设置允许的请求方式
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                // 设置允许的header属性
                .allowedHeaders("*")
                // 跨域允许时间
                .maxAge(3600);
    }
}