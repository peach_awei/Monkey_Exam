package com.example.monkey_exam.controller;

import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.entity.pojo.Notice;
import com.example.monkey_exam.service.ExamService;
import com.example.monkey_exam.service.NoticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@Slf4j
@RequestMapping("/exam")
@PreAuthorize("hasAnyAuthority('teacher','admin')")
public class ExamController {

    @Autowired
    ExamService examService;

    @Autowired
    NoticeService noticeService;

    /**
     * 发布考试
     * 请求路径：/addexam
     * 请求方式：post
     */
    @PostMapping("/addexam")
    public Result AddExam(@RequestBody Exam exam){
        log.info("新增考试：{}" ,exam);
//        发布考试
        examService.AddExam(exam);
//        封装通知内容
        Notice notice = new Notice();
        notice.setNoticeHeadline("考试通知");
        String time = String.valueOf(exam.getExTime());
        notice.setNoticeContent("关于 "+ time.replace("T"," ") +" 组织进行 "+exam.getExName()+" 请各位同学准时参加，诚信参考！祝各位考试考出水平");
        notice.setNoticeLevel(0);
        notice.setNoticeTeacherId(exam.getExTeacherId());
        notice.setNoticeStatus(0);
        notice.setNoticeClassId(exam.getExClassId());
        notice.setNoticeTime(LocalDateTime.now());
        notice.setNoticeEndTime(exam.getExEndtime());
//        通知学生

        return Result.success("已新增" + noticeService.newNotice(notice) + "条记录");
    }

    /**
     * 分页查询考场
     * 请求路径：/page
     * 请求方式：Get
     */
//    @PostMapping("/page")
//    public PageDTO PageOfExam(@RequestBody Map<String,Object> resultMap){
//        Integer page = 1;
//        if(resultMap.get("page") != null && !resultMap.get("page").equals(""))
//        {
//            page = (Integer) resultMap.get("page");
//        }
//
//        Integer pageSize = 7;
//        if(resultMap.get("pageSize") != null && !resultMap.get("pageSize").equals(""))
//        {
//            pageSize = (Integer) resultMap.get("pageSize");
//        }
//        Integer tId = (Integer) resultMap.get("teacherId");
//        String exName = (String) resultMap.get("exName");
//        Integer classId = (Integer) resultMap.get("classId");
//        LocalDate startTime = (LocalDate) resultMap.get("startTime");
//        return examService.Page(page, pageSize,tId, exName, classId, startTime);
//    }

    /**
     * 分页查询考场
     * 请求路径：/page
     * 请求方式：Get
     */
    @GetMapping("/page")
    public PageDTO PageOfExam(@RequestParam(defaultValue = "1") Integer page,
                              @RequestParam(defaultValue = "7") Integer pageSize,
                              Integer teacherId, String exName, Integer classId,
                              @RequestParam(required = false) String startTime){
        return examService.Page(page, pageSize, teacherId, exName, classId, startTime);
    }

    /**
     * 删除考试
     * 请求路径：/delete
     * 请求方式：Delete
     */
    @DeleteMapping("/delete")
    public Result Delete(Integer exId){
        return Result.success("已删除" + examService.del(exId) + "条记录");
    }

    /**
     * 修改考试
     * 请求路径：/update
     * 请求方式：Put
     */
    @PutMapping("/update")
    public Result update(@RequestBody Exam exam){
        return Result.success("已修改" + examService.updateByexId(exam) + "条记录");
    }


}
