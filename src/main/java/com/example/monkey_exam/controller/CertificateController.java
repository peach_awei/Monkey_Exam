package com.example.monkey_exam.controller;

import com.example.monkey_exam.Util.AliOSSUtils;
import com.example.monkey_exam.Util.CertificateGenerator_2;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Certificate;
import com.example.monkey_exam.service.CertificateService;
import com.example.monkey_exam.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/certificate")
public class CertificateController {

    @Autowired
    CertificateService certificateService;

    @Autowired
    StudentService studentService;

    @Autowired
    AliOSSUtils aliOSSUtils;



    /**
     * 查看班级成员
     * 请求地址：/getmember
     * 请求方式：Get
     */
    @GetMapping("/getmember")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result getMember(Integer classId){
        return Result.success(studentService.memberList(classId));
    }


    /**
     * 生成证书
     * 请求地址：/generate
     * 请求方式：Post
     */
    @PostMapping("/generate")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result generate(@RequestBody Certificate certificate){
//        记录时间
        LocalDateTime local = LocalDateTime.now();
        certificate.setCertTime(local);
        log.info("{}",certificate);
//
        return Result.success(certificateService.insert(certificate));
    }


    /**
     * 删除证书
     * 请求地址：/del
     * 请求方式：Delete
     */
    @DeleteMapping("/del")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result Del(Integer certId) throws Exception {
        String re = certificateService.getCert(certId).getCertFile();
        if (re == null){
            certificateService.Del(certId);
        }else {
            String fileName = certificateService.getCert(certId).getCertFile();
            System.out.println(fileName.substring(47));
            Integer result = aliOSSUtils.del(fileName.substring(47));
            if (result==1){
                return Result.success("已删除" + certificateService.Del(certId) + "份证书（包括云）");
            }else {
                return Result.fail("云未删除");
            }
        }
        return Result.success("普通删除证书（因为没有生成）");
    }


    /**
     * 学生-查看自己的所有证书
     * 请求地址：/searchall
     * 请求方式：Get
     * 使用分页
     */
    @GetMapping("/searchall")
    @PreAuthorize("hasAuthority('student')")
    public Result searchAll(@RequestParam(defaultValue = "1") Integer page,
                            @RequestParam(defaultValue = "7") Integer pageSize,
                            Integer certStuId){
        return Result.success(certificateService.getAllCert(page, pageSize, certStuId));
    }


    /**
     * 学生-生成证书和链接
     * 第一次创建证书（需要生成到本地，再将文件上传云）
     * 第二次只需要在上一步中获取下载地址即可
     * 需要将云地址上传sql数据库
     * 请求地址：/download
     * 请求方式：Get
     */
    @GetMapping("/download")
    @PreAuthorize("hasAuthority('student')")
    public Result download(Integer certId) throws Exception {

        Certificate certificate = certificateService.getCert(certId);
        //        封装
        Map<String, String> data = new HashMap<>();
//        证书内容：
        data.put("Name", certificate.getCertStuName());
        System.out.println(certificate.getCertStuName());
        if (certificate.getCertContent().length() > 28){
            data.put("Content", certificate.getCertContent().substring(0,28));    //28个字符
            data.put("Content2", certificate.getCertContent().substring(28));   //28个字符
        }else {
            data.put("Content", certificate.getCertContent());
        }

        data.put("WardName", certificate.getCertWardName());
        data.put("TeacherName", certificate.getCertTeaName());
        data.put("Date", String.valueOf(certificate.getCertTime().toLocalDate()));
//        证书存放地址：
//        data.put("Data", "D:\\Project\\other\\Certificate\\");
//        调用方法生成证书，并记录地址
        String file = CertificateGenerator_2.GeneratePdf(data);
        // 创建File对象
        File file1 = new File(file);
        // 获取文件名和扩展名
        String fileName = file1.getName();
        String httpUrl = aliOSSUtils.upload(fileName,file);
        certificateService.updateFile(certId,httpUrl);
        return Result.success(httpUrl);
    }


}
