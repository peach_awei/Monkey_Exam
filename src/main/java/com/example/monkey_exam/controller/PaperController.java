package com.example.monkey_exam.controller;


import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.dto.PaperDTO;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Bank;
import com.example.monkey_exam.entity.pojo.Eq;
import com.example.monkey_exam.entity.pojo.Paper;
import com.example.monkey_exam.service.BankService;
import com.example.monkey_exam.service.EqService;
import com.example.monkey_exam.service.PaperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/paper")
@PreAuthorize("hasAnyAuthority('teacher','admin')")
public class PaperController {

    @Autowired
    PaperService paperService;
    @Autowired
    BankService bankService;
    @Autowired
    EqService eqService;


    /**
     * 分页查询考试卷
     * 请求路径：/page
     * 请求方式：Get
     * 使用分页
     */
    @PostMapping("/page")
    public PageDTO PageOfPaper(@RequestParam(defaultValue = "1")Integer page,
                               @RequestParam(defaultValue = "2")Integer pageSize,
                               @RequestParam(required = false) String paperName,
                               @RequestParam(required = false) LocalDateTime startTime,
                               @RequestParam(required = false) LocalDateTime endTime){
        return paperService.Page(page, pageSize, paperName, startTime, endTime);
    }


    /**
     * 删除试卷
     * 请求路径：/del
     * 请求方式：Delete
     */
    @DeleteMapping("/del")
    public Result delPaper(Integer paperId){
        eqService.deleteById(null, paperId);
        return Result.success("已删除" + paperService.deletePaper(paperId) + "条记录");
    }


    /**
     * 创建试卷
     * 请求路径：/insert
     * 请求方式：Post
     */
    @PostMapping("/insert")
    public Result insertPaper(@RequestBody PaperDTO paperDTO){
//        创建考试
        paperDTO.getPaper().setPaperCreateTime(LocalDateTime.now());
        Integer paperId = paperService.insertPaper(paperDTO.getPaper());
        Eq eq = new Eq();
        eq.setEqPaperId(paperId);
//        试卷加题
        if(!paperDTO.getCheckList().isEmpty()){
            for (Integer id : paperDTO.getCheckList()) {
                eq.setEqTestId(id);
                eq.setEqTestType(1);
                eq.setEqTestGrade(paperDTO.getGrade().get(0));
                eqService.insertOne(eq);
            }
        }
        if(!paperDTO.getCheckBoxList().isEmpty()){
            for (Integer id : paperDTO.getCheckBoxList()) {
                eq.setEqTestId(id);
                eq.setEqTestType(2);
                eq.setEqTestGrade(paperDTO.getGrade().get(1));
                eqService.insertOne(eq);
            }
        }
        if(!paperDTO.getCheckPanList().isEmpty()){
            for (Integer id : paperDTO.getCheckPanList()) {
                eq.setEqTestId(id);
                eq.setEqTestType(3);
                eq.setEqTestGrade(paperDTO.getGrade().get(2));
                eqService.insertOne(eq);
            }
        }if(!paperDTO.getCheckTianList().isEmpty()){
            for (Integer id : paperDTO.getCheckTianList()) {
                eq.setEqTestId(id);
                eq.setEqTestType(4);
                eq.setEqTestGrade(paperDTO.getGrade().get(3));
                eqService.insertOne(eq);
            }
        }
        if(!paperDTO.getCheckJianList().isEmpty()){
            for (Integer id : paperDTO.getCheckJianList()) {
                eq.setEqTestId(id);
                eq.setEqTestType(5);
                eq.setEqTestGrade(paperDTO.getGrade().get(4));
                eqService.insertOne(eq);
            }
        }
        return Result.success("成功创建试卷与题目");
    }


    /**
     * 点击提交试卷后，记录试卷并更新分数
     * 请求路径：/update
     * 请求方式：Put
     */
    @PutMapping("/update")
    public Result updatePaper(@RequestBody Paper paper){
        return Result.success("已更新" + paperService.updatePaper(paper) + "条记录");
    }


    //----------------------------------------------------------------------------------
    //                            以下是对试卷里的题目进行操作                              |
    //----------------------------------------------------------------------------------

    /**
     *  查找试卷里的所有题
     * 请求路径：/searchpaper
     * 请求方式：Get
     */
    @GetMapping("/searchpaper")
    public Result searchPaper(Integer paperId){
        return Result.success(eqService.selectTestById(paperId));
    }


    /**
     * 1.往试卷里添加题目（添加题库里没有的题）
     *  需要先将题目存进bank表
     * 2.在eq表里记录题目
     * 请求路径：/add
     * 请求方式：Post
     */
//    @PostMapping("/newadd")
//    public Result newAddTest(@RequestBody Map<String,String> resultMap){
////        接收参数
//        Integer paperId = Integer.valueOf(resultMap.get("paperId"));
//        Integer grade = Integer.valueOf(resultMap.get("grade"));
//        Bank bank = new Bank();
//        bank.setBankTeacherId(Integer.parseInt(resultMap.get("bankTeacherId")));
//        bank.setBankTestName(resultMap.get("bankTestName"));
//        bank.setBankTestType(String.valueOf(TestTypeConverter.convertType(resultMap.get("bankTestType"))));
//        bank.setBankTestSubject(resultMap.get("bankTestSubject"));
//        bank.setBankTestAnswer(resultMap.get("bankTestAnswer"));
//        bank.setBankTestSectionA(resultMap.get("bankTestSectionA"));
//        bank.setBankTestSectionB(resultMap.get("bankTestSectionB"));
//        bank.setBankTestSectionC(resultMap.get("bankTestSectionC"));
//        bank.setBankTestSectionD(resultMap.get("bankTestSectionD"));
//        bank.setBankTestSectionE(resultMap.get("bankTestSectionE"));
//        bank.setBankTestSectionF(resultMap.get("bankTestSectionF"));
//        bank.setBankTestSectionG(resultMap.get("bankTestSectionG"));
//        bank.setBankTestAnalyze(resultMap.get("bankTestAnalyze"));
////        添加
//        int id = bankService.AddTestByTeaid(bank);
//        Eq eq = new Eq();
//        eq.setEqPaperId(paperId);
//        eq.setEqTestId(id);
//        eq.setEqTestGrade(grade);
//        return Result.success("已添加" + eqService.insertOne(eq) + "条记录");
//    }


    /**
     *  查找bank里的所有题
     * 请求路径：/searchall
     * 请求方式：Get
     */
    @GetMapping("/searchall")
    public Result searchAll(Integer bankTeacherId){
        List<Bank> bankList = bankService.SeacheAllTest(bankTeacherId);
        return Result.success(bankList);
    }


    /**
     * 1.往试卷里添加题目（添加题库里的题）
     *  查找bank里的所有题
     * 2.在eq表里记录题目
     * 请求路径：/addform
     * 请求方式：Post
     */
//    @PostMapping("/addform")
//    public Result addTestFrom(@RequestBody List<Eq> eq){
//        return Result.success("已添加" + eqService.insertEq(eq) + "条记录");
//    }


    /**
     * 修改已经添加了的题目
     * 请求路径：/changetest
     * 请求方式：Put
     */
    @PutMapping("/changetest")
    public Result changeTest(@RequestBody Bank bank){
        return Result.success("已修改" + bankService.UpdateTestByTid(bank) + "条记录");
    }


    /**
     * 删除已经添加的题目（eq表）
     * 请求路径：/deltest
     * 请求方式：Delete
     */
    @DeleteMapping("/deltest")
    public Result changeTest (Integer eqTestId, Integer eqPaperId){
        return Result.success("已删除" + eqService.deleteById(eqTestId,eqPaperId) + "条记录");
    }

    /**
     * 获取所有试卷
     * 请求路径：/allpaper
     * 请求方式：Get
     */
    @GetMapping("/allpaper")
    public Result allPaper (Integer teaId){
        return Result.success(paperService.allPaper(teaId));
    }

}