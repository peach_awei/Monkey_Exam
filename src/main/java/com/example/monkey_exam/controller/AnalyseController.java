package com.example.monkey_exam.controller;

import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.dto.RecordAnalyseDTO;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Class;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.entity.vo.ExamAnalyseVo;
import com.example.monkey_exam.entity.vo.RecordAnalyseVo;
import com.example.monkey_exam.service.ClassService;
import com.example.monkey_exam.service.ExamService;
import com.example.monkey_exam.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/analyse")
@PreAuthorize("hasAnyAuthority('teacher','admin')")
public class AnalyseController {

    @Autowired
    ExamService examService;

    @Autowired
    ClassService classService;

    @Autowired
    RecordService recordService;

    /**
     * 查看自己管理的班级列表
     * 请求路径：/searchclass
     * 请求方式：Get
     */
    @GetMapping("/searchclass")
    public Result searchClass(Integer teaId){
        List<Class> classList = classService.selectClass(teaId);
        List<Map<String, Object>> resultList = classList.stream()
                .map(c -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("classId", c.getClassId());
                    map.put("className", c.getClassName());
                    return map;
                })
                .collect(Collectors.toList());
        return Result.success(resultList);
    }


    /*
     * 显示我发布的所有考试（正进行的考试+考完的考试）
     * 请求路径：/findexamed
     * 请求方式：Get
     * TODO 解决传来的date  "2024-06-04T16:00:00.000Z", "2024-07-08T16:00:00.000Z"
     * 使用分页
     * */
    @GetMapping("/findexamed")
    public PageDTO<Exam> findExamed(@RequestParam(defaultValue = "1") Integer page,
                                    @RequestParam(defaultValue = "7") Integer pageSize,
                                    Integer teaId, String exName, Integer classId, String startDate, String endDate){
//        LocalDate startDate = null;
//        LocalDate endDate = null;
//        if(Date != null && !Date.equals("")){
//            startDate = LocalDate.parse(Date.substring(1,11));
//            endDate = LocalDate.parse(Date.substring(29,39));
//        }
        

//        LocalDateTime startDate = LocalDateTime.parse(Date.substring(1,20).replace("T"," "));
//        LocalDateTime endDate = LocalDateTime.parse(Date.substring(29,48).replace("T"," "));

        System.out.println(startDate);
        System.out.println(endDate);
        return examService.pageAnalyseExam(page, pageSize, teaId, exName, classId, startDate, endDate);
    }


    /*
     * 分析考试
     * 请求路径：/satistics
     * 请求方式：Get
     * */
    @GetMapping("/satistics")
    public Result Statistics(Integer exId, Integer paperId){
//        分析表
//        查询
        ExamAnalyseVo examAnalyse = examService.analyseExam(exId);
        RecordAnalyseVo recordAnalyse = recordService.recordAnalyse(exId, paperId);
//        封装
        RecordAnalyseDTO recordAnalyseDTO = new RecordAnalyseDTO();
        recordAnalyseDTO.setExamAnalyse(examAnalyse);
        recordAnalyseDTO.setRecordAnalyse(recordAnalyse);
        return Result.success(recordAnalyseDTO);
    }


    /*
     * 分析考试分数  -->  饼图
     * 请求路径：/satistics
     * 请求方式：Get
     * */
    @GetMapping("/cookiemap")
    public Result cookieMap(Integer exId){
        return Result.success(recordService.gradeList(exId));
    }
}
