package com.example.monkey_exam.controller;

import com.example.monkey_exam.converter.TestTypeConverter;
import com.example.monkey_exam.entity.dto.PageBean;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Bank;
import com.example.monkey_exam.service.BankService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/bank")
@PreAuthorize("hasAnyAuthority('teacher','admin')")
public class BankController {
    //  该类大部分使用mybatis，小部分使用mybatis-plus

    @Autowired
    BankService bankService;


    /**
     * 请求路径：/type
     * 请求方式：Get
     * 查找题目类型
     */
    @GetMapping("/type")
    public List<String> type(){
        return bankService.getType();
    }


    /**
     * 请求路径：/page
     * 请求方式：Get
     * 分页查询所有题目(分页条件查询：试题类型，题型，题干)
     */
    @GetMapping("/page")
    public Result page(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "7") Integer pageSize,
                       Integer teaId, String name, String type, String subject){
        Integer converterType = null;
        if (type!=null && !type.isEmpty()){
            converterType = TestTypeConverter.convertType(type);
        }
//        查询
        PageBean pageBean = bankService.page(page, pageSize, teaId, name, converterType, subject);
        return Result.success(pageBean);
    }


    /**
     * 删除题库题目
     * 请求路径：/deltest
     * 请求方式：Delete
     */
    @PostMapping("/deltest")
    public Result DeleteTest(@RequestBody List<Integer> ids){
        return Result.success("已删除" + bankService.DeleteTestById(ids) + "条数据");
    }


    /**
     * 增加新题目
     * 请求路径：/addtest
     * 请求方式：Post
     */
    @PostMapping("/addtest")
    public Result AddTest(@RequestBody Bank bank){
        bank.setBankTestType(String.valueOf(TestTypeConverter.convertType(bank.getBankTestType())));
        int id = bankService.AddTestByTeaid(bank);
        return Result.success("已新增,id为" + id + "的记录");
    }

    /**
     * 根据tid和teaid查找题目
     * 请求路径：/search
     * 请求方式：Get
     */
    @GetMapping("/search")
    public Result SearchTestByName(Integer bankTestId, Integer bankTeacherId){
        return Result.success(bankService.SearchTestById(bankTestId, bankTeacherId));
    }


    /**
     * 通过tid修改题目
     * 请求路径：/update
     * 请求方式：Put
     */
    @PutMapping("/update")
    public Result UpdateTest(@RequestBody Bank bank) {
        if (bank.getBankTestType().length()>1){
            bank.setBankTestType(String.valueOf(TestTypeConverter.convertType(bank.getBankTestType())));
        }
        return Result.success("已修改" + bankService.UpdateTestByTid(bank) + "条数据");
    }

}
