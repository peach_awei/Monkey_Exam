package com.example.monkey_exam.controller;


import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.dto.SheetDTO;
import com.example.monkey_exam.entity.pojo.Answer;
import com.example.monkey_exam.entity.pojo.Class;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.entity.pojo.Record;
import com.example.monkey_exam.entity.vo.ExamListVo;
import com.example.monkey_exam.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/record")
@PreAuthorize("hasAnyAuthority('teacher','admin')")
public class RecordController {

    @Autowired
    RecordService recordService;

    @Autowired
    ExamService examService;

    @Autowired
    AnswerService answerService;

    @Autowired
    ClassService classService;

    @Autowired
    StudentService studentService;

    //----------------------------------按考试查询批阅试卷---------------------------------
    /*
     * 1.显示我发布的所有考试（正进行的考试+考完的考试）
     * 请求路径：/findexamed
     * 请求方式：Get
     * 使用分页
     * */
    @GetMapping("/findexamed")
    public PageDTO<Exam> findExamed(@RequestParam(defaultValue = "1") Integer page,
                                    @RequestParam(defaultValue = "7") Integer pageSize,
                                    Integer teaId){
        return examService.PageFindExamed(page,pageSize,teaId);
    }


    /*
     * 2.根据exid在record表查找哪些人提交了考试
     * 2.1.需要展示学号带名字
     * q:需要在实体类里加入一个数据库不存在的字段
     * 请求路径：/findwho
     * 请求方式：Get
     * 使用分页
     * */
    @GetMapping("/findwho")
    public PageDTO<Record> findWho(@RequestParam(defaultValue = "1") Integer page,
                                   @RequestParam(defaultValue = "7") Integer pageSize,
                                   Integer exId){
        return recordService.pageFindWho(page, pageSize, exId);
    }


    /*
     * 3.根据stuid在answer表查找该学生的答题卡
     *  3.1.展示：试卷名 学生名字 试卷分数 是否及格 判卷人 （record表+student表+exam表） Map<>
     *  3.2.展示：题目 选项 正确答案 学员答案 题目解释说明 试题分数 （bank表+answer表） list<map<>>
     *  3.3.实现：仅查看错题
     * `    only:只查看错题
     * 请求路径：/findcard
     * 请求方式：Get
     * */
    @GetMapping("/findcard")
    public SheetDTO<Map> findCard(Integer stuId, Integer exId, Integer only){
        SheetDTO<Map> dto = new SheetDTO();

        Map<String, Object> examInfo = recordService.findCard(stuId, exId);
        dto.setExamInfo(examInfo);

        List<Map> cardInfo = answerService.cardInfo(stuId, exId, only);
        dto.setCardList(cardInfo);
        return dto;
    }


    /*
     * 4.根据选择的answerid，答题卡里的题目批阅：
     *  4.1.对错对应题的满分和0分，也可以单独修改分数(由前端负责)
     * 请求路径：/changegrade
     * 请求方式：Put
     * */
    @PutMapping("/changegrade")
    public Result changeGrade(@RequestBody Answer answer){
        return Result.success("已修改" + answerService.changeGrade(answer.getAnswerStuId(), answer.getAnswerExamId(), answer.getAnswerTestId(), answer.getAnswerGrade()) + "条记录");
    }


    /*
     * 老师点击批阅完毕后，更新record的批改状态和批阅时间 计算学生得分
     * 请求路径：/changestatus
     * 请求方式：Put
     * */
    @PutMapping("/changestatus")
    public Result changeStatus(@RequestBody Record record){
//        计算得分
        Double grade = answerService.sumGrade(record.getRecordStuId(), record.getRecordExamId());
//        修改表
        return Result.success("已修改" + recordService.changeStatus(record.getRecordStuId(), record.getRecordExamId(), grade) + "条记录");
    }


    //----------------------------------按考生查询批阅试卷---------------------------------

    /*
     * 1.查找自己管理的班级
     * 请求路径：/findclass
     * 请求方式：Get
     * */
    @GetMapping("/findclass")
    public Result findClass(Integer classTeacherId){
        List<Class> classList = classService.selectClass(classTeacherId);
        return Result.success(classList);
    }


    /*
     * 2.查看班级里的成员，及参与考试次数
     * 请求路径：/userlist
     * 请求方式：Get
     * */
    @GetMapping("/userlist")
    public Result userList(Integer classId){
        List<Map<String,Object>> classList = studentService.selectByClassId(classId);
        return Result.success(classList);
    }


    /*
     * 3.查看学生的考试记录（考试名，考试总分，考生成绩，是否及格，交卷时间，批改状态）
     * 请求路径：/examlist
     * 请求方式：Get、
     * 需要分页
     * */
    @GetMapping("/examlist")
    public PageDTO<ExamListVo> examList(@RequestParam(defaultValue = "1") Integer page,
                                        @RequestParam(defaultValue = "7") Integer pageSize,
                                        Integer stuId){
        return recordService.examList(page, pageSize, stuId);
    }

    /*
     * 按照考生查询批阅的展示答题卡部分和批阅打分部分。
     *
     * 和前面的步骤一样，直接传哪场考试的哪场学生。
     *
     * 直接调用接口就行
     * */

}
