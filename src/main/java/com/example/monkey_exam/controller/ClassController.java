package com.example.monkey_exam.controller;

import com.example.monkey_exam.Util.RandomCode;
import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Class;
import com.example.monkey_exam.entity.pojo.Notice;
import com.example.monkey_exam.entity.pojo.Student;
import com.example.monkey_exam.service.ClassService;
import com.example.monkey_exam.service.NoticeService;
import com.example.monkey_exam.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/class")
@PreAuthorize("hasAnyAuthority('teacher','admin')")
public class ClassController{

    @Autowired
    ClassService classService;

    @Autowired
    StudentService studentService;

    @Autowired
    NoticeService noticeService;


    /**
     * 创建班级
     * 请求路径：/insert
     * 请求方式：post
     */
    @PostMapping("/insert")
    public Result insertClass(@RequestBody Class c){
        Integer inviteCode = RandomCode.inviteCode();
        c.setClassInvite(inviteCode);
        classService.insertClass(c);
        return Result.success(inviteCode);
    }


    /**
     * 删除班级
     * 1.删除班级
     * 2.发布公告
     * 请求路径：/del
     * 请求方式：Delete
     */
   @DeleteMapping("/del")
    public Result delClass(Integer classId, Integer classTeacherId){
//       通知
       Notice newnotice = new Notice();
       newnotice.setNoticeHeadline("班级变动通知：");
       String className = classService.selectById(classId).getClassName();
       newnotice.setNoticeContent("你的老师解散了班级："+ className +"。请联系老师获取班级代码后，前往个人中心加入班级！");
       newnotice.setNoticeLevel(1);
       newnotice.setNoticeTeacherId(classTeacherId);
       newnotice.setNoticeStatus(0);
       newnotice.setNoticeClassId(classId);
       newnotice.setNoticeEndTime(LocalDateTime.now().plusDays(2));
       noticeService.newNotice(newnotice);
//      删除
       return Result.success("已删除" + classService.delClass(classId) + "条记录");
   }


    /**
     * 修改班级名字
     * 请求路径：/updateClassNameById
     * 请求方式：Put
     */
    @PutMapping("/updateClassNameById")
    public Result updateClassNameById(@RequestBody Class c){
        return Result.success("已修改：" + classService.updateClassNameById(c.getClassId(),c.getClassName()) + "条记录");
    }


    /**
     * 查看自己管理的班级列表
     * 请求路径：/searchclass
     * 请求方式：Get
     */
    @GetMapping("/searchclass")
    public Result searchClass(Integer classTeacherId){
        List<Class> classList = classService.selectClass(classTeacherId);
        return Result.success(classList);
    }

    /**
     * 查看班级信息（包括学生信息）
     * 请求路径：/searchclassinfor
     * 请求方式：Get
     * 使用分页查询
     */
    @GetMapping("/searchclassinfor")
    public PageDTO<Student> searchClassInfor(@RequestParam(defaultValue = "1") Integer page,
                                             @RequestParam(defaultValue = "7") Integer pageSize,
                                             Integer classId){
        return classService.Page(page, pageSize, classId);
    }

    /**
     * 初始化学员密码
     * 请求路径：/changepsw
     * 请求方式：Put
     */
    @PutMapping("/changepsw")
    public Result changePsw(@RequestBody List<Integer> stuids){
        //代表初始化密码
        String status = null;
        return Result.success("已修改：" + studentService.updatePsw(stuids, null, status) + "条记录");
    }


    /**
     * 踢出学生
     * 请求路径：/delstu
     * TODO class里的学生数量 -1
     * 请求方式：Put
     */
    @PutMapping("/delstu")
    public Result delStu(@RequestBody Student student){
        Integer status = 0;   //设置学生的班级为0，即无班级
        classService.minusNum(studentService.selectOnesInfo(Integer.valueOf(student.getStuId())).getStuClass());
        return Result.success("已踢出：" + studentService.changeClass(Integer.valueOf(student.getStuId()),status)+"名学生");
    }

}
