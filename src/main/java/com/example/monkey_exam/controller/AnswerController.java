package com.example.monkey_exam.controller;

import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Answer;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.service.AnswerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/answer")
@PreAuthorize("hasAuthority('student')")
public class AnswerController {

    @Autowired
    AnswerService answerService;


//    -----------------------------学生错题本功能------------------------------

    /*
     * 查找有错题的考试
     * 请求路径：/searchwrong
     * 请求方式：Get
     * */
    @GetMapping("/search_wrong")
    public Result searchWrong(Integer answerStuId){
//        通过id查询有错题的考试，获取该考试的信息
        List<Exam> exams = answerService.selectExam(answerStuId);
        return Result.success(exams);
    }


    /*
    * 查找用户本次考试的错题
    * 请求路径：/findzero
    * 请求方式：Get
    * */
    @GetMapping("/find_zero")
    public Result select(Integer answerStuId, Integer answerExamId){
        List<LinkedHashMap<String, Object>> answerList = answerService.findZero(answerStuId, answerExamId);
        return Result.success(answerList);
    }

}
