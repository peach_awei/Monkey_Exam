package com.example.monkey_exam.controller;

import com.example.monkey_exam.entity.dto.PageDTO;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Teacher;
import com.example.monkey_exam.service.TeacherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/manager")
@PreAuthorize("hasAuthority('admin')")
public class ManagerController {

    @Autowired
    TeacherService teacherService;

    /**
     * 生成教师账号
     * 请求地址：/newteacher
     * 请求方式：Post
     */
    @PostMapping("/newteacher")
    public Result newTeacher(@RequestBody Teacher teacher) {
        teacher.setTeacherStatus(0);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        teacher.setTeacherPassword(passwordEncoder.encode("123456"));
        return Result.success("已新增" + teacherService.newTeacher(teacher) + "条记录");
    }


    /**
     * 分页查询普通教师
     * 请求地址：/page
     * 请求方式：Get
     */
    @GetMapping("/page")
    public PageDTO Page(@RequestParam(defaultValue = "1") Integer page,
                        @RequestParam(defaultValue = "7") Integer pageSize,
                        Integer teaId, String teaName, String gender){
        if (gender != null && !gender.isEmpty()){
            if (gender.equals("男")){
                gender = "1";
            }else {
                gender = "0";
            }
        }
        return teacherService.pageteacher(page, pageSize, teaId, teaName, gender);
    }

    /**
     * 重设教师密码
     * 请求地址：/repassword
     * 请求方式：Post
     */
    @PostMapping("/repassword")
    public Result rePassword(@RequestBody List<Integer> tids) {
        return Result.success("已重设" + teacherService.rePassword(tids) + "条记录");
    }


}
