package com.example.monkey_exam.controller;

import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Notice;
import com.example.monkey_exam.service.NoticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/notice")
@PreAuthorize("hasAnyAuthority('teacher','admin')")
public class NoticeController {

    @Autowired
    NoticeService noticeService;

    /*
     * 查询通知
     * 请求路径：/searchnotice
     * 请求方式：Get
     * */
    @GetMapping("/searchnotice")
    @PreAuthorize("hasAnyAuthority('teacher','admin','student')")
    public Result searchNotice(Integer noticeClassId){
        return Result.success(noticeService.selectall(noticeClassId));
    }


    /*
     * 创建通知
     * 请求路径：/newnotice
     * 请求方式：Post
     * */
    @PostMapping("/newnotice")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result newNotice(@RequestBody Notice notice){
        return Result.success("已新增" + noticeService.newNotice(notice) + "条记录");
    }


    /*
     * 修改通知
     * 请求路径：/changenotice
     * 请求方式：Put
     * */
    @PutMapping("/changenotice")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result changeNotice(@RequestBody Notice notice){
        return Result.success("已修改" + noticeService.changeNotice(notice) + "条记录");
    }

    /*
     * 删除通知
     * 请求路径：/delete
     * 请求方式：Delete
     * */
    @DeleteMapping("/delete")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result delete(Integer noticeId){
        return Result.success("已删除" + noticeService.delete(noticeId) + "条记录");
    }

}
