package com.example.monkey_exam.controller;

import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.pojo.Teacher;
import com.example.monkey_exam.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@Slf4j
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    TeacherService teacherService;

    @Autowired
    ExamService examService;

    @Autowired
    BankService bankService;

    @Autowired
    RecordService recordService;

    @Autowired
    StudentService studentService;

    @Autowired
    private LoginService loginService;

    //    -----------------------------教师登录------------------------------

    @PostMapping("/login")
    public Result login(@RequestBody Teacher teacher) {
        //登录
        return loginService.loginTeacher(teacher);
    }

//-------------------------------教师首页功能--------------------------------

    /**
     * 首页_查询总题目数量
     * 请求路径：/countall
     * 请求方式：Get
     */
    @GetMapping("/countalltest")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result countAllTest(Integer teaId){
        return Result.success(bankService.countAll(teaId));
    }



    /**
     * 首页_查询总考试数量
     * 请求路径：/countallexam
     * 请求方式：Get
     */
    @GetMapping("/countallexam")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result countAll(Integer teaId){
        return Result.success(examService.countAll(String.valueOf(teaId)));
    }


    /**
     * 首页_查询正在进行的考试
     * 请求路径：/countexaming
     * 请求方式：Get
     */
    @GetMapping("/countexaming")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result countExaming(Integer teaId){
        return Result.success(examService.countExaming(teaId));
    }

    /**
     * 首页_查询参与情况
     * 请求路径：/participation
     * 请求方式：Get
     */
    @GetMapping("/participation")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result Participation(Integer teaId){
        return Result.success(recordService.Participation(teaId));
    }


    //-------------------------------个人--------------------------------
    /**
     * 完善个人信息
     * 请求地址：/myinformation
     * 请求方式：Post
     */
    @PostMapping("/myinformation")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result myInformation(@RequestBody Teacher teacher) {
//        男-1，女-0
        return Result.success("已修改：" + teacherService.myInformation(Integer.valueOf(teacher.getTeaId()), teacher.getTeacherAge(), teacher.getTeacherGender()) + "条记录");
    }


    /**
     * 修改密码
     * 请求地址：/changepsw
     * 请求方式：Post
     */
    @PostMapping("/changepsw")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    public Result changePsw(@RequestBody Map<String ,Object> teacher){
        Integer teaId = (Integer) teacher.get("Id");
        String old = (String) teacher.get("Old");
        String password = (String) teacher.get("Password");
        Teacher ss = teacherService.myInforWithPsw(teaId);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        teacher.put("Password" , passwordEncoder.encode(password));
        if (passwordEncoder.matches(old, ss.getTeacherPassword())){
            return Result.success("已更改" + teacherService.changePsw(teacher)+ "条记录");
        }
        return Result.fail("新旧密码不正确");
    }

    //-----------------------------------退出登录--------------------------------

    @GetMapping("/logout")
    @PreAuthorize("hasAnyAuthority('teacher','admin')")
    //ResponseResult是我们在domain目录写好的实体类
    public Result logout(){
        return loginService.logoutTeacher();
    }


//    //-------------------------------分析统计--------------------------------
//             //--------------------按学员分析----------------------
//    /**
//     * 分页查询学生列表
//     * 请求地址：/student
//     * 请求方式：Get
//     * 使用分页条件查询
//     */
//    @GetMapping("/student")
//    public PageDTO Student(Integer page, Integer pagesize, Integer tid, String sname, Integer classid, Integer gender){
//        return studentService.userPage(page, pagesize, tid, sname, classid, gender);
//    }
//
//
//    /**
//     * 根据获取到的id统计考试然后分析
//     * 请求地址：/countbysid
//     * 请求方式：Get
//     */
//    @GetMapping("/countbysid")
//    public Result countBySid(){
//        return Result.success();
//    }
//
//    /**
//     * 成绩分析
//     * 请求地址：/countbysid
//     * 请求方式：Get
//     */
//
//    /**
//     * 排名分析
//     * 请求地址：/countbysid
//     * 请求方式：Get
//     */
//
//
//             //--------------------按考试分析----------------------
//    /**
//     * 分页查询学生列表
//     * 请求地址：/exam
//     * 请求方式：Get
//     * 使用分页条件查询
//     */
//    @GetMapping("/exam")
//    public PageDTO Exam(Integer page, Integer pagesize, Integer tid, String exName, Integer classId, LocalDateTime startTime){
//        return examService.Page(page, pagesize, tid, exName, classId, startTime);
//    }
//
//    /**
//     * 根据获取到的id统计考试然后分析
//     * 请求地址：/countbysid
//     * 请求方式：Get
//     */
//    @GetMapping("/countbyExid")
//    public Result countByExid(){
//        return Result.success();
//    }
//
//
//    /**
//     * 成绩分析
//     * 请求地址：/countbysid
//     * 请求方式：Get
//     */
//
//    /**
//     * 试题分析
//     * 请求地址：/countbysid
//     * 请求方式：Get
//     */

}
