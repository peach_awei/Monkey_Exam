package com.example.monkey_exam.controller;

import com.example.monkey_exam.Util.RandomCode;
import com.example.monkey_exam.Util.SendSms;
import com.example.monkey_exam.entity.dto.AnswerRecordDTO;
import com.example.monkey_exam.entity.dto.Result;
import com.example.monkey_exam.entity.dto.SheetDTO;
import com.example.monkey_exam.entity.pojo.Student;
import com.example.monkey_exam.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@RestController
@Slf4j
@RequestMapping("/student")
public class StudentController {

    @Autowired
    NoticeService noticeService;

    @Autowired
    ExamService examService;

    @Autowired
    RecordService recordService;

    @Autowired
    EqService eqService;

    @Autowired
    AnswerService answerService;

    @Autowired
    StudentService studentService;

    @Autowired
    ClassService classService;

    @Autowired
    private LoginService loginService;

    //    -----------------------------学生登录------------------------------

    @PostMapping("/login")
    public Result login(@RequestBody Student student) {
        //登录
        return loginService.loginStudent(student);
    }

    //    -----------------------------学生注册------------------------------

    private String code ;

    @PostMapping("/sms")
    public Result sms(@RequestBody Student student) throws Exception {
        code = RandomCode.main().toString();
       SendSms.main(student.getStuPhone(),code);
       return Result.success();
    }


    @PostMapping("/register")
    public Result register(@RequestBody Student student) {
        if(student.getCode().equals(code)){
            code = null;
//            通过邀请码获取班级id
            Integer classId = classService.getClassId(student.getInvite());
            student.setStuClass(classId);
//            班级人数 + 1
            classService.addNum(classId);
//            注册学生
            return Result.success(studentService.register(student));
        }
        return Result.fail("验证码错误");
    }

    //    -----------------------------学生首页------------------------------

    /**
     * 1.1.对于公告结束日期 < 当前日期的公告 修改其公告状态
     * 1.2.展示公告
     * 1.2.1.该板块应包括展示全部，前端可设置首页展示2-3条，公告包括内容和标题。
     * 展示全部时，再展示公告全部内容
     * 请求地址：/notice
     * 请求方式：Get
     */
    @GetMapping("/indexnotice")
    @PreAuthorize("hasAuthority('student')")
    public Result indexNotice(Integer stuClass) {
//        1.
        noticeService.updateStatus(stuClass);
//        2.
        return Result.success(noticeService.findNotice(stuClass));
    }


    /**
     * 2.展示马上进行的考试和正在进行的考试
     * 请求地址：/indexexam
     * 请求方式：Get
     */
    @GetMapping("/indexexam")
    @PreAuthorize("hasAuthority('student')")
    public Result indexExam(Integer stuClass) {
        return Result.success(examService.selectExam(stuClass));
    }


     /**
      * 3.查看自己最近一次考试的排名
      * 请求地址：/indexrank
      * 请求方式：Get
      */
     @GetMapping("/indexrank")
     @PreAuthorize("hasAuthority('student')")
    public Result indexRank(Integer stuId) {
//         查找该生最近的一场考试
         Integer exid = recordService.searchRecentExam(stuId);
        return Result.success(recordService.searchRank(stuId,exid));
     }


//    -----------------------------学生考试功能------------------------------

    //    ----------------全部考试-------------------
    /**
     * 1.查看所有即将进行和正在进行的考试
     * 请求地址：/myexam
     * 请求方式：Get
     */
    @GetMapping("/myexam")
    @PreAuthorize("hasAuthority('student')")
    public Result myExam(Integer stuClass) {
        return Result.success(examService.selectExam(stuClass));
    }


    /**
     * 2.点击考试开始答题
     *    通过exid获取试卷内容
     * 请求地址：/examing
     * 请求方式：Get
     */
    @GetMapping("/examing")
    @PreAuthorize("hasAuthority('student')")
    public SheetDTO Examing(Integer exId, Integer paperId) {
        //          查找考试id与名称、使用的试卷id
        LinkedHashMap<String, Object> examInfo = examService.findExamInfo(exId);
        Timestamp timestamp = (Timestamp) examInfo.get("ex_time");
        LocalDateTime time = timestamp.toLocalDateTime();
        if (LocalDateTime.now().isAfter(time)){
            SheetDTO<Map> dto = new SheetDTO();
//      封装考试信息
            dto.setExamInfo(examInfo);
//      封装答题卡
            List<LinkedHashMap<String, Object>> cardInfo = eqService.selectTestById(paperId);
            dto.setCardList(cardInfo);
            return dto;
        }else {
            SheetDTO<Map> dto = new SheetDTO();
            dto.setExamInfo("未到考试时间");
            return dto;
        }

    }


    /**
     * 3.答题并交卷
     *  操作answer表
     *  写入考试记录到record
     * 请求地址：/answering
     * 请求方式：Get
     */
    @PostMapping("/answering")
    @PreAuthorize("hasAuthority('student')")
    public Result Answering(@RequestBody AnswerRecordDTO data) {
        recordService.addRecord(data.getRecord());
        return Result.success("提交题目数量：" + answerService.Answering(data.getAnswer()));
    }


    //    ----------------历史考试-------------------
    /**
     * 1.查看自己已经参加了的考试
     * 请求地址：/myexamed
     * 请求方式：Get
     */
    @GetMapping("/myexamed")
    @PreAuthorize("hasAuthority('student')")
    public Result myExamed(Integer stuId){
        return Result.success(examService.selectExamed(stuId));
    }


    /**
     * 2.查看答题卡
     * 根据stuid，exid查询已经批阅的试卷答题卡
     * 请求地址：/myexamed
     * 请求方式：Get
     */
    @GetMapping("/record")
    @PreAuthorize("hasAuthority('student')")
    public SheetDTO record(Integer stuId, Integer exId) {
        SheetDTO<Map> dto = new SheetDTO<>();

        Map<String, Object> examInfo = recordService.findCard(stuId, exId);
        dto.setExamInfo(examInfo);

        List<Map> cardInfo = answerService.cardInfo(stuId, exId, null);
        dto.setCardList(cardInfo);
        return dto;
    }


    //    ----------------个人-------------------


    /**
     * 加入班级
     * 请求地址：/join
     * TODO class 学生数量 +1
     * 请求方式：Put
     */
    @PutMapping("/join")
    @PreAuthorize("hasAuthority('student')")
    public Result join(@RequestBody Student student){
//        比对数据库里的和传来的邀请码是否一致
        if (student.getInvite().matches(String.valueOf(classService.selectById(student.getStuClass()).getClassInvite()))){
            classService.addNum(student.getStuClass());
            return Result.success("已更改" + studentService.changeClass(Integer.valueOf(student.getStuId()),student.getStuClass()) + "条记录");
        }else {
            return Result.fail("错误的邀请码或邀请码不存在");
        }
    }


    /**
     * 退出当前班级
     * 请求地址：/exit
     * 请求方式：Put
     */
    @PutMapping("/exit")
    @PreAuthorize("hasAuthority('student')")
    public Result exit(@RequestBody Student student){
        classService.minusNum(student.getStuClass());
                                                                                            //设置学生的班级为0，即无班级
        return Result.success("已更改" + studentService.changeClass(Integer.valueOf(student.getStuId()),0) + "条记录");
    }


    /**
     * 查看个人信息
     * 请求地址：/info
     * 请求方式：Get
     */
    @GetMapping("/info")
    @PreAuthorize("hasAuthority('student')")
    public Result info(Integer stuId){
        return Result.success(studentService.selectOnesInfo(stuId));
    }


    /**
     * 修改个人密码
     * 请求地址：/changepsw
     * 请求方式：Get
     */
    @PostMapping("/changepsw")
    @PreAuthorize("hasAuthority('student')")
    public Result changePsw(@RequestBody Map<String ,Object> student){
        Integer stuId = (Integer) student.get("Id");
        String old = (String) student.get("Old");
        String password = (String) student.get("Password");
        Student ss = studentService.selectOnesInfo(stuId);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (passwordEncoder.matches(old, ss.getStuPassword())){
            return Result.success("已更改" + studentService.updatePsw(null, stuId,password)+ "条记录");
        }
        return Result.fail("新旧密码不正确");
    }

    /**
     * 查找是否存在此用户
     * 请求地址：/forget
     * 请求方式：Post
     */
    @GetMapping("/forget")
    public Result forget(Integer stuId, Integer stuPhone){
        Student exit = studentService.selectOneExit( stuId, stuPhone);
        if(exit != null){
            return Result.success("用户存在");
        }
        return Result.fail("用户不存在");
    }

    /**
     * 忘记密码的短信验证码
     * 请求地址：/message
     * 请求方式：Get
     */

    private String codeInPsw;

    @PostMapping("/message")
    public Result message(@RequestBody Student student) throws Exception {
        codeInPsw = RandomCode.main().toString();
        SendSms.main(student.getStuPhone(),codeInPsw);
        return Result.success();
    }


    /**
     * 忘记密码
     * 请求地址：/forgetpsw
     * 请求方式：Get
     */
    @PostMapping("/forgetpsw")
    public Result forgetPsw(@RequestBody Student student) {
        if (codeInPsw.equals(student.getCode())) {
            return Result.success("已更改" + studentService.updatePsw(null, Integer.valueOf(student.getStuId()), student.getStuPassword()) + "条记录");
        }
        return Result.fail("验证码不正确");
    }

    //    ---------------------------公告-----------------------------

    @GetMapping("/allnotice")
    @PreAuthorize("hasAuthority('student')")
    public Result allNotice(Integer stuClass){
        return Result.success(noticeService.findNotice(stuClass));
    }


    @GetMapping("/onenotice")
    @PreAuthorize("hasAuthority('student')")
    public Result oneNotice(Integer noticeId){
        return Result.success(noticeService.oneNotice(noticeId));
    }


    //-----------------------------------退出登录--------------------------------

    @GetMapping("/logout")
    @PreAuthorize("hasAuthority('student')")
    //ResponseResult是我们在domain目录写好的实体类
    public Result logout(){
        return loginService.logoutStudent();
    }
}