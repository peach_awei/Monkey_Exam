package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Certificate;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CertificateMapper extends BaseMapper<Certificate>{
}
