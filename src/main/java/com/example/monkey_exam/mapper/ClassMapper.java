package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Class;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ClassMapper extends BaseMapper<Class> {
    @Select("SELECT class_id FROM class WHERE class_teacher_id = #{tid}")
    List<Integer> selectId(Integer tid);

    @Update("UPDATE class SET class_num = class_num - 1 WHERE class_id = #{stuClass}")
    Integer minusNum(int stuClass);

    @Update("UPDATE class SET class_num = class_num + 1 WHERE class_id = #{stuClass}")
    Integer addNum(int stuClass);
}
