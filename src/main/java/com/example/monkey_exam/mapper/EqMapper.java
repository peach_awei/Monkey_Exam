package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Eq;
import org.apache.ibatis.annotations.Select;

import java.util.LinkedHashMap;
import java.util.List;

public interface EqMapper extends BaseMapper<Eq> {
    @Select("SELECT b.bank_test_id, b.bank_test_name, e.eq_test_grade, b.bank_test_type, b.bank_test_section_A, b.bank_test_section_B, " +
            "b.bank_test_section_C, b.bank_test_section_D, b.bank_test_section_E, b.bank_test_section_F, b.bank_test_section_G " +
            "FROM bank b " +
            "JOIN eq e ON b.bank_test_id = e.eq_test_id " +
            "WHERE e.eq_paper_id = #{paperid} " +
            "ORDER BY b.bank_test_type")
    List<LinkedHashMap<String,Object>> selectTestById(Integer paperid);

    @Select("SELECT eq_test_grade FROM eq WHERE eq_test_id =#{answerTestId} AND eq_paper_id=#{answerPaperId}")
    Double getGrade(Integer answerTestId,Integer answerPaperId);
}
