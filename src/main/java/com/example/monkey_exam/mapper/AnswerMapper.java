package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Answer;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface AnswerMapper extends BaseMapper<Answer> {
    @Select("SELECT b.bank_test_name, b.bank_test_type, b.bank_test_answer,b.bank_test_section_A, " +
            "       b.bank_test_section_B, b.bank_test_section_C, b.bank_test_section_D, b.bank_test_section_E, " +
            "       b.bank_test_section_F, b.bank_test_section_G, b.bank_test_analyze, a.answer_your " +
            "FROM bank b " +
            "JOIN answer a ON b.bank_test_id = a.answer_test_id " +
            "WHERE a.answer_stu_id = #{userId} AND a.answer_exam_id= #{examId} AND a.answer_grade = 0 " +
            "ORDER BY b.bank_test_type ASC")
    List<LinkedHashMap<String, Object>> findZero(Integer userId, Integer examId);

    @MapKey("stuid")
    List<Map> cardInfo(Integer stuid, Integer exid, Integer only);

    @Select("SELECT sum(answer_grade), answer_stu_id, answer_exam_id " +
            "FROM answer " +
            "WHERE answer_stu_id = #{recordStuId} AND answer_exam_id = #{recordExamId}")
    Double sumGrade(Integer recordStuId, Integer recordExamId);
}
