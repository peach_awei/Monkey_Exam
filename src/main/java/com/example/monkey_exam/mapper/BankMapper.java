package com.example.monkey_exam.mapper;

import com.example.monkey_exam.entity.pojo.Bank;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BankMapper {

    //    分页条件查询题库所有题目
    List<Bank> List(Integer teaid, String name, Integer type, String subject);

    //    查询所有题目数量
    @Select("SELECT COUNT(*) FROM bank WHERE bank_teacher_id = #{teaid}")
    Integer count(Integer teaid);

    //    分页查询，获取列表数据
    @Select("SELECT * FROM bank LIMIT #{start},#{size}")
    List<Bank> page(Integer start, Integer size);

    //    通过id删除题库中的题
    Integer DeleteTestById(List<Integer> ids);

    //    新增题目
    @Insert("INSERT INTO bank (bank_teacher_id, bank_test_time, bank_test_name, bank_test_type, bank_test_subject, bank_test_answer, " +
            " bank_test_section_A, bank_test_section_B, bank_test_section_C, bank_test_section_D, bank_test_section_E, bank_test_section_F, bank_test_section_G, bank_test_analyze)" +
            " values (#{bankTeacherId}, #{bankTestTime},#{bankTestName},#{bankTestType},#{bankTestSubject},#{bankTestAnswer}," +
            "#{bankTestSectionA},#{bankTestSectionB},#{bankTestSectionC},#{bankTestSectionD},#{bankTestSectionE},#{bankTestSectionF},#{bankTestSectionG}," +
            "#{bankTestAnalyze})")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "bankTestId", before = false, resultType = Integer.class)
    //statement是要运行的SQL语句，它的返回值通过resultType来指定
    //before=true，插入之前进行查询，可以将查询结果赋给keyProperty和keyColumn，赋给keyColumn相当于更改数据库
    //使用selectKey，并且使用MySQL的last_insert_id()函数时，before必为false，也就是说必须先插入然后执行last_insert_id()才能获得刚刚插入数据的ID
    @Options(useGeneratedKeys = true, keyProperty = "bankTestId")
    int AddTestByTeaid(Bank bank);

    //    根据题目和教师工号查找题目
    @Select("SELECT * " +
            "FROM bank WHERE bank_test_id = #{tid} AND bank_teacher_id = #{teaid}")
    Bank SearchTestById(Integer tid,Integer teaid);

    //    根据题目和id修改题目
    @Update("UPDATE bank SET bank_test_name=#{bankTestName}, bank_test_type=#{bankTestType}, bank_test_subject=#{bankTestSubject}, " +
            "bank_test_answer=#{bankTestAnswer}, bank_test_section_A=#{bankTestSectionA}, bank_test_section_B=#{bankTestSectionB}, " +
            "bank_test_section_C=#{bankTestSectionC}, bank_test_section_D=#{bankTestSectionD}, bank_test_section_E=#{bankTestSectionE}, " +
            "bank_test_section_F=#{bankTestSectionF}, bank_test_section_G=#{bankTestSectionG}, bank_test_analyze=#{bankTestAnalyze} " +
            "WHERE bank_test_id = #{bankTestId}")
    int UpdateTestByTid(Bank bank);

    @Select("SELECT * FROM bank WHERE bank_teacher_id = #{bank.teaid}")
    List<Bank> SearchAllTest(Integer teaid);

    @Select("SELECT bank_test_subject FROM bank GROUP BY bank_test_subject ")
    List<String> getType();

    @Select("SELECT bank_test_answer FROM bank WHERE bank_test_id = #{answerTestId}")
    String getRight(int answerTestId);
}
