package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Exam;
import com.example.monkey_exam.entity.vo.ExamAnalyseVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.LinkedHashMap;
import java.util.List;

@Mapper
public interface ExamMapper extends BaseMapper<Exam> {
    @Select("SELECT e.ex_id, e.ex_name, e.ex_paper_id, e.ex_teacher_id, e.ex_class_id, e.ex_time, e.ex_endtime, p.paper_pass_grade, p.paper_grade " +
            "FROM exam e " +
            "JOIN paper p ON e.ex_paper_id = p.paper_id " +
            "WHERE ex_id = #{exid}")
    LinkedHashMap<String, Object> findExamInfo(Integer exid);

    @Select("SELECT e.ex_id, e.ex_name, e.ex_paper_id, e.ex_teacher_id, t.teacher_name, e.ex_class_id, e.ex_time, e.ex_endtime " +
            "FROM exam e " +
            "JOIN teacher t ON e.ex_teacher_id = t.tea_id " +
            "WHERE e.ex_endtime < localtime AND e.ex_teacher_id = #{teaid} " +
            "ORDER BY e.ex_time ASC")
    List<Exam> selectExamList(Integer teaid);

    @Select("SELECT e.ex_name, e.ex_class_id, c.class_name, p.paper_pass_grade, p.paper_grade " +
            "FROM exam e " +
            "JOIN class c ON e.ex_class_id = c.class_id " +
            "JOIN paper p ON e.ex_paper_id = p.paper_id " +
            "WHERE e.ex_id = #{exId}")
    ExamAnalyseVo analyzeExam(Integer exId);

    @Select("SELECT e.ex_id, e.ex_name, p.paper_grade, p.paper_pass_grade, r.record_grade, r.record_start_time, r.record_end_time " +
            "FROM exam e " +
            "JOIN record r ON e.ex_id = r.record_exam_id " +
            "JOIN paper p ON e.ex_paper_id = p.paper_id " +
            "WHERE r.record_stu_id = #{stuId}")
    List<LinkedHashMap<String,Object>> selectExam(Integer stuId);
}