package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.monkey_exam.entity.pojo.Record;
import com.example.monkey_exam.entity.vo.ExamListVo;
import com.example.monkey_exam.entity.vo.ParticipationVo;
import com.example.monkey_exam.entity.vo.RecordAnalyseVo;
import com.example.monkey_exam.entity.vo.RecordRankVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface RecordMapper extends BaseMapper<Record> {
    @Select("SELECT r.record_id, r.record_stu_id, s.stu_name, r.record_exam_id, r.record_teacher_id, " +
            "r.record_status, r.record_pass_grade, r.record_grade, r.record_start_time, r.record_end_time " +
            "FROM record r " +
            "JOIN student s ON r.record_stu_id = s.stu_id " +
            "WHERE r.record_exam_id = #{exid} " +
            "ORDER BY r.record_end_time ASC")
    List<Record> pageFindWho(Integer exid);


    @Select("SELECT r.record_stu_id, s.stu_name, r.record_exam_id, e.ex_name, r.record_teacher_id, " +
            "r.record_status, r.record_pass_grade, r.record_grade, r.record_teacher_id, t.teacher_name " +
            "FROM record r " +
            "JOIN exam e ON r.record_exam_id = e.ex_id " +
            "JOIN student s ON r.record_stu_id = s.stu_id " +
            "JOIN teacher t ON r.record_teacher_id = t.tea_id " +
            "WHERE r.record_stu_id = #{stuid} AND r.record_exam_id = #{exid} " +
            "ORDER BY r.record_id ASC ")
    Map<String, Object> selectCard(Integer stuid, Integer exid);


    @Select("SELECT e.ex_id, e.ex_name, e.ex_paper_id, p.paper_pass_grade, p.paper_grade, r.record_end_time, r.record_status " +
            "FROM record r " +
            "JOIN exam e ON r.record_exam_id = e.ex_id " +
            "JOIN paper p  ON e.ex_paper_id = p.paper_id " +
            "WHERE r.record_stu_id = #{stuid} " +
            "ORDER BY r.record_end_time ASC ")
    IPage<ExamListVo> examList(Page<ExamListVo> page1, Integer stuid);

    @Select("SELECT record_exam_id " +
            "FROM record " +
            "WHERE record_stu_id = #{stuid} " +
            "ORDER BY record_start_time " +
            "LIMIT 1")
    Integer searchRecentExam(Integer stuid);

    List<RecordRankVo> searchRank(Integer stuid, Integer exid);

    @Select("SELECT DATE(record_end_time) AS time, COUNT(*) AS num " +
            "FROM record " +
            "WHERE record_teacher_id = #{teaid} " +
            "GROUP BY time ")
    List<ParticipationVo> participation(Integer teaid);

    @Select("SELECT record_exam_id, " +
            "    AVG(record_grade) AS average_score, " +
            "    MAX(record_grade) AS max_score, " +
            "    MIN(record_grade) AS min_score, " +
            "    COUNT(*) AS participant_count, " +
            "    SUM(CASE WHEN record_grade > (SELECT paper_pass_grade FROM paper WHERE paper_id = #{paperId}) THEN 1 ELSE 0 END) AS pass_count, " +
            "    CAST(SUM(CASE WHEN record_grade > (SELECT paper_pass_grade FROM paper WHERE paper_id = #{paperId}) THEN 1 ELSE 0 END) AS FLOAT)/CAST(COUNT(*) AS FLOAT) * 100 AS pass_rate " +
            "FROM record " +
            "WHERE record_exam_id = #{exId} ")
    RecordAnalyseVo recordAnalyse(Integer exId, Integer paperId);

    @Select("SELECT " +
            "    (SELECT COUNT(*) FROM record WHERE record_exam_id = #{exId} AND record_grade = 100) AS '100分的人数', " +
            "    (SELECT COUNT(*) FROM record WHERE record_exam_id = #{exId} AND record_grade >= 90 AND record_grade < 100) AS '90分以上的人数', " +
            "    (SELECT COUNT(*) FROM record WHERE record_exam_id = #{exId} AND record_grade >= 60 AND record_grade < 90) AS '60分以上的人数', " +
            "    (SELECT COUNT(*) FROM record WHERE record_exam_id = #{exId} AND record_grade < 60) AS '60分以下的人数';")
    Map<String,Integer> gradeList(Integer exId);
}
