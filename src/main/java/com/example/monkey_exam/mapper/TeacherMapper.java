package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Teacher;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TeacherMapper extends BaseMapper<Teacher> {
}
