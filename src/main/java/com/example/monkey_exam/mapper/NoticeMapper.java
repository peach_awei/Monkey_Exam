package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.LinkedHashMap;
import java.util.List;

@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {
    @Select("SELECT n.notice_id, n.notice_headline, n.notice_content, n.notice_level, n.notice_teacher_id, t.teacher_name, " +
            "n.notice_class_id, c.class_name, n.notice_time "+
            "FROM notice n " +
            "LEFT JOIN teacher t ON t.tea_id = n.notice_teacher_id " +
            "LEFT JOIN class c ON c.class_id = n.notice_class_id " +
            "WHERE n.notice_class_id = #{classid} " +
            "AND n.notice_status = 0 " +
            "ORDER BY n.notice_time DESC")
    List<LinkedHashMap<String, Object>> selectAll(Integer classid);
}
