package com.example.monkey_exam.mapper;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.monkey_exam.entity.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface StudentMapper extends BaseMapper<Student> {
    @Select("SELECT s.stu_id, s.stu_name, COUNT(r.record_exam_id) AS count_exam " +
            "FROM student s " +
            "JOIN record r ON s.stu_id = r.record_stu_id " +
            "WHERE s.stu_class = #{classid} " +
            "GROUP BY s.stu_id, s.stu_name ")
    List<Map<String,Object>> selectUserList(Integer classid);
}
