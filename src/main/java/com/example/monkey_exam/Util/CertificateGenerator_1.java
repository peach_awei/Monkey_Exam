package com.example.monkey_exam.Util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CertificateGenerator_1 {

            public static void main(String[] args) {
                String backgroundImagePath = "C:/Users/28059/Downloads/ot1.png"; // 奖状背景图片路径
                String outputImagePath = "aaa.png"; // 输出图片路径
                String wardname = "“最佳MVP”"; // 奖项
                String name = "";
                String date = "2024-04-11"; // 日期
                String classRoom = "xxx"; // 班级

                // 创建带有背景图片的奖状
                try {
                    BufferedImage backgroundImage = ImageIO.read(new File(backgroundImagePath));
                    int width = backgroundImage.getWidth();
                    int height = backgroundImage.getHeight();
                    BufferedImage certificate = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                    Graphics2D g2d = certificate.createGraphics();
                    g2d.drawImage(backgroundImage, 0, 0, null);

                    // 设置文本属性
                    Font font = new Font("思源宋体", Font.BOLD, 32);
                    g2d.setFont(font);
                    g2d.setColor(Color.BLACK);

                    // 添加文本内容
                    String title = name + "同学";
                    Font titleFont = new Font("宋体", Font.BOLD, 48);
                    g2d.setFont(titleFont);
                    FontMetrics metrics = g2d.getFontMetrics();
                    int titleX = (width - metrics.stringWidth(title)) / 12;
                    int titleY = 510; // 距离顶部的距离
                    g2d.drawString(title, titleX, titleY);

                    String mvpText = "在今日分享中取得____的最高分，被评为“最佳MVP”，相信在今后的求职路上";
                    g2d.setFont(font);
                    //int mvpX = (width - metrics.stringWidth(mvpText)) / 1;
                    int mvpY = titleY + 70; // 距离标题的距离
                    g2d.drawString(mvpText, 120, mvpY);

                    String mvp2Text = "依然会保持好心态，勇往直前";
                    g2d.setFont(font);
                    int mvp2X = (width - metrics.stringWidth(mvp2Text)) / 9;
                    int mvp2Y = mvpY + 50; // 距离标题的距离
                    g2d.drawString(mvp2Text, mvp2X, mvp2Y);

                    String nameText = "" + wardname;
                    Font nameFont = new Font("宋体", Font.ITALIC, 100);
                    g2d.setFont(nameFont);
                    g2d.setColor(Color.RED);
                    int nameX = (width - metrics.stringWidth(nameText)) / 3;
                    int nameY = mvpY + 160; // 距离MVP文本的距离
                    g2d.drawString(nameText, nameX, nameY);

                    String dateText = "日期: " + date;
                    Font dateFont = new Font("宋体", Font.BOLD, 40);
                    g2d.setFont(dateFont);
                    g2d.setColor(Color.BLACK);
                    int dateX = (width - metrics.stringWidth(dateText)) / 1;
                    int dateY = nameY + 100; // 距离姓名文本的距离
                    g2d.drawString(dateText, dateX, dateY);

                    String classRoomText = "班级: " + classRoom;
                    //int classRoomX = (width - metrics.stringWidth(classRoomText)) / 1;
                    int classRoomY = dateY + 60; // 距离日期文本的距离
                    g2d.drawString(classRoomText, 955, classRoomY);

                    // 释放资源
                    g2d.dispose();

                    // 保存生成的奖状图片
                    ImageIO.write(certificate, "PNG", new File(outputImagePath));
                    System.out.println("奖状已生成至: " + outputImagePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
