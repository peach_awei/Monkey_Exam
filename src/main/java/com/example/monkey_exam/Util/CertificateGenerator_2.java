package com.example.monkey_exam.Util;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.*;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class CertificateGenerator_2 {
    public static String GeneratePdf(Map<String, String> data) throws IOException, DocumentException {

//         导入PDF模板
        String fileName = "https://monkeying.oss-cn-hangzhou.aliyuncs.com/CertificateWithVar.pdf";

//         生成的新文件路径
        String newPDFPath = data.get("Name") + "_" + data.get("WardName") +".pdf";
//        String newPDFPath = data.get("Data") + data.get("Name") + "_" + data.get("WardName") +".pdf";

//        上传云端


        try {
//            1.读取模板
            PdfReader reader = new PdfReader(fileName);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

//             2.读取PDF模板内容
            PdfStamper ps = new PdfStamper(reader, bos);
            PdfContentByte under = ps.getUnderContent(1);

//            3.使用项目下的自定义的中文字体
            BaseFont bf = BaseFont.createFont( "STSongStd-Light" ,"UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
            Font blodFont= new Font(bf, 12,Font.NORMAL);

//             4.获取模板中的所有字段
            AcroFields fields = ps.getAcroFields();
            fields.addSubstitutionFont(blodFont.getBaseFont());


            fillData(fields, data); //调用方法执行写入

//             必须要调用这个，否则文档不会生成的
            ps.setFormFlattening(true);
            ps.close();

//             5.将要生成的目标PDF文件名称
            OutputStream fos = new FileOutputStream(newPDFPath);
            fos.write(bos.toByteArray());
            fos.flush();
            fos.close();
            bos.close();
        } catch (IOException e) {
            System.out.println("证书生成失败,原因:" + e.getLocalizedMessage());
        } catch (DocumentException e) {
            System.out.println("证书生成失败,原因:" + e.getLocalizedMessage());
        }

        return newPDFPath;
    }

    // 获取pdf模板中有哪些字段key+赋值的值value
    public static void fillData(AcroFields fields, Map<String, String> data) throws IOException, DocumentException {
        for (String key : data.keySet()) {
            String value = data.get(key); // 调用data方法获取值
            fields.setField(key, value); // 为字段赋值,注意字段名称是区分大小写的
        }
    }

}
